//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_SDL_DRAW_H
#define HEADER_SDL_DRAW_H

#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "settings.hpp"
#include "error.hpp"

class GUI_Button;

//  class implements move and zoom
class Move_Zoom
{
private:
	int view_x_offset = 0;
	int view_y_offset = 300;

	const float_32 ZOOM_STEP = 0.09f;
	const float_32 MAX_ZOOM_OUT = 0.16f;
#ifdef DEBUG
	const float_32 MAX_ZOOM_IN = 9.0f;
#else
	const float_32 MAX_ZOOM_IN = 4.0f;
#endif
	float_32 view_zoom = 1.5f;

public:
	Move_Zoom(){};
	virtual ~Move_Zoom(){};

	void center_level(int level_width){
		if(level_width > 0)
		{
			// center view to level width
			int window_w = Settings::singleton()->window_width;
			view_zoom = float_32(window_w) / float_32(level_width);
			view_x_offset = int(float_32(level_width / 2)*view_zoom  - float_32(window_w / 2));
			view_y_offset = int( 450.0f / view_zoom);
		}
	};

	// move and zoom functions
	int get_view_x_offset() const { return view_x_offset; }

	int get_view_y_offset() const { return view_y_offset; }

	float_32 get_view_zoom() const { return view_zoom; }

	void move_up()   { view_y_offset += Settings::singleton()->window_height / 16;  }
	void move_down() { view_y_offset -= Settings::singleton()->window_height / 16; }
	void move_left() { view_x_offset += Settings::singleton()->window_width / 16; }
	void move_right(){ view_x_offset -= Settings::singleton()->window_width / 16; }
	void move_to(float_32 x, float_32 y){
		view_x_offset = int( float_32(Settings::singleton()->window_width) / 2.0f / view_zoom - x);
		view_y_offset = int( float_32(Settings::singleton()->window_height) / 2.0f / view_zoom + y);
	}

	void zoom_in(){
		float_32 center_x = screen_to_world_x(Settings::singleton()->window_width/2);
		float_32 center_y = screen_to_world_y(Settings::singleton()->window_height/2);

		// maximal zoom in
		if ( view_zoom < MAX_ZOOM_IN) view_zoom += ZOOM_STEP;

		view_x_offset -= int( center_x - screen_to_world_x(Settings::singleton()->window_width/2));
		view_y_offset -= int( screen_to_world_y(Settings::singleton()->window_height/2) - center_y);
	}

	void zoom_out(){
		float_32 center_x = screen_to_world_x(Settings::singleton()->window_width/2);
		float_32 center_y = screen_to_world_y(Settings::singleton()->window_height/2);

		// maximal zoom out
		if ( view_zoom > MAX_ZOOM_OUT) view_zoom -= ZOOM_STEP;

		view_x_offset -= int( center_x - screen_to_world_x(Settings::singleton()->window_width/2));
		view_y_offset -= int( screen_to_world_y(Settings::singleton()->window_height/2) - center_y);
	}


	// convert world (float_32) to screen coordinates (int)
	int world_to_screen_x(float_32 x) {
		return int((x + float_32(view_x_offset)) * view_zoom);
	}

	int world_to_screen_y(float_32 y) {
		return int((-y + float_32(view_y_offset)) * view_zoom);
	}

	// convert screen coordinates (int) to world (float_32)
	float_32 screen_to_world_x (int x) {
		return (float_32(x) / view_zoom) - float_32(view_x_offset);
	}

	float_32 screen_to_world_y (int y) {
		return (float_32(-y) / view_zoom) + float_32(view_y_offset);
	}
};

// abstract class as drawing Interface
class IDraw
{
public:
	IDraw(){};
	virtual ~IDraw(){};

	// virtual draw functions
	virtual void draw_background(SDL_Color c) =0;
	virtual void draw_background_image(std::string filename) = 0;
	virtual void draw_particle(float_32 x, float_32 y, float radius , SDL_Color c) =0;
	virtual void draw_spring(float_32 ax, float_32 ay, float_32 bx, float_32 by, float line_width, SDL_Color c) =0;
	virtual void draw_broken_spring(float_32 ax, float_32 ay, float_32 bx, float_32 by, float line_width, SDL_Color c) = 0;
	virtual void draw_ground(const float_32 ground[], int level_width, SDL_Color c) =0;
	virtual void draw_water(float_32 water_height, SDL_Color c) =0;
	virtual void draw_ground_editbox(float_32 x, float width, float_32 ground_height, SDL_Color box_c, SDL_Color ground_c) =0;
	virtual void draw_grid(const SDL_Color c_most, const SDL_Color c_some) =0;
	virtual void draw_button(GUI_Button* button) =0;
	virtual void draw_rect(const float_32 x, const float_32 y, const float_32 width, const float_32 height, const SDL_Color c) =0;

	// wrapper for filledPolygonRGBA (SDL_Renderer *renderer, const Sint16 *vx, const Sint16 *vy, int n, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
	virtual void draw_train(const float_32* vx, const float_32* vy, const int n, const SDL_Color tc) =0;

	// uses screen coordinates x,y
	virtual void draw_string(int x, int y, const char* str, SDL_Color c) =0;

	virtual void draw_pentagram(const float_32 x, const float_32 y, const float_32 width, const SDL_Color c) =0;



};

// Interface for different drawing engines
class IDraw_Move_Zoom : public IDraw , public Move_Zoom
{
public:
	IDraw_Move_Zoom(){};
	virtual ~IDraw_Move_Zoom(){};
};

/// draw and zoom with SDL internal renderer (software or hardware)
class SDL_draw : public IDraw_Move_Zoom
{
private:
	SDL_Renderer* renderer;

public:
	SDL_draw(SDL_Renderer* ren)
		: renderer(ren) {}
	~SDL_draw(){};

	void draw_background(SDL_Color c);
	void draw_background_image(std::string filename);

	void draw_particle(float_32 x, float_32 y, float radius , SDL_Color c);
	void draw_spring(float_32 ax, float_32 ay, float_32 bx, float_32 by, float line_width, SDL_Color c);
	void draw_broken_spring(float_32 ax, float_32 ay, float_32 bx, float_32 by, float line_width, SDL_Color c);
	void draw_ground(const float_32 ground[], int level_width, SDL_Color c);
	void draw_water(float_32 water_height, SDL_Color c);
	void draw_ground_editbox(float_32 x, float width, float_32 ground_height, SDL_Color box_c, SDL_Color ground_c);
	void draw_grid(const SDL_Color c_most, const SDL_Color c_some);
	void draw_train(const float_32 *vx, const float_32 *vy, const int n, const SDL_Color tc);
	void draw_button(GUI_Button* button);
	void draw_rect(const float_32 x, const float_32 y, const float_32 width, const float_32 height, const SDL_Color c);
	// uses screen coordinates x,y
	void draw_string(int x, int y, const char* str, SDL_Color c);
	void draw_pentagram(const float_32 x, const float_32 y, const float_32 width, const SDL_Color c);

	// help functions (only in SDL_draw with SDL internal renderer)
	SDL_Texture* render_text(const std::string& text, const std::string& font_filename, const SDL_Color c, const int font_size);
};

/// draw and zoom with SDL opengl (very fast and recommended)
class SDL_opengl_draw : public IDraw_Move_Zoom
{
private:
	// pointer to SDL OpenGL textures
	GLuint opengl_bg_texture = 0;
public:
	SDL_opengl_draw(){};
	~SDL_opengl_draw(){};

	void draw_background(SDL_Color c);
	void draw_background_image(std::string filename);

	void draw_particle(float_32 x, float_32 y, float radius , SDL_Color c);
	void draw_spring(float_32 ax, float_32 ay, float_32 bx, float_32 by, float line_width, SDL_Color c);
	void draw_broken_spring(float_32 ax, float_32 ay, float_32 bx, float_32 by, float line_width, SDL_Color c);
	void draw_ground(const float_32 ground[], int level_width, SDL_Color c);
	void draw_water(float_32 water_height, SDL_Color c);
	void draw_ground_editbox(float_32 x, float width, float_32 ground_height, SDL_Color box_c, SDL_Color ground_c);
	void draw_grid(const SDL_Color c_most, const SDL_Color c_some);
	void draw_train(const float_32 *vx, const float_32 *vy, const int n, const SDL_Color tc);
	void draw_button(GUI_Button* button);
	void draw_rect(const float_32 x, const float_32 y, const float_32 width, const float_32 height, const SDL_Color c);
	// uses screen coordinates x,y
	void draw_string(int x, int y, const char* str, SDL_Color c);
	void draw_pentagram(const float_32 x, const float_32 y, const float_32 width, const SDL_Color c);

	void draw_button_background(SDL_Color c, float x, float y, float x2, float y2);

	// returns opengl texture number and texture pixel width and height
	GLuint text_to_texture(const std::string& text, const std::string& font_filename, const SDL_Color c, const int font_size, int& w, int& h);
	GLuint text_to_texture_bicolor(const std::string& text, const std::string& font_filename, const SDL_Color c1, const SDL_Color c2, const int req_font_size, int button_w, int button_h, int& result_w, int& result_h);

	void draw_rounded_corner(float x, float y, float sa, float arc, float r);
};

#endif
