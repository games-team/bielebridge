//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HEADER_HASH_H
#define HEADER_HASH_H

#include <cstdint>

// Derived from the public domain Skein reference implementation
// (credits to the Skein team: see http://www.skein-hash.info/ )
// DO NOT COPY THIS CODE AND USE IT FOR CRYPTOGRAPHIC PURPOSE
// THIS VERSION HAS BEEN SIMPLIFIED AND IS PROBABLY INSECURE
// It is meant to prevent cheating by editing budget, levels or config

/*****************************************************************
** "Internal" Skein definitions
**    -- not needed for sequential hashing API, but will be
**           helpful for other uses of Skein (e.g., tree hash mode).
**    -- included here so that they can be shared between
**           reference and optimized code.
******************************************************************/

/* tweak word T[1]: bit field starting positions */
#define SKEIN_T1_BIT(BIT)       ((BIT) - 64)            /* offset 64 because it's the second word  */
#define SKEIN_T1_POS_BIT_PAD    SKEIN_T1_BIT(119)       /* bit  119     : partial final input byte */
#define SKEIN_T1_POS_BLK_TYPE   SKEIN_T1_BIT(120)       /* bits 120..125: type field               */
#define SKEIN_T1_POS_FIRST      SKEIN_T1_BIT(126)       /* bits 126     : first block flag         */
#define SKEIN_T1_POS_FINAL      SKEIN_T1_BIT(127)       /* bit  127     : final block flag         */
/* tweak word T[1]: flag bit definition(s) */
#define SKEIN_T1_FLAG_FIRST     (((uint64_t)  1 ) << SKEIN_T1_POS_FIRST)
#define SKEIN_T1_FLAG_FINAL     (((uint64_t)  1 ) << SKEIN_T1_POS_FINAL)
#define SKEIN_T1_FLAG_BIT_PAD   (((uint64_t)  1 ) << SKEIN_T1_POS_BIT_PAD)

/* tweak word T[1]: block type field */
#define SKEIN_BLK_TYPE_KEY      ( 0)                    /* key, for MAC and KDF */
#define SKEIN_BLK_TYPE_CFG      ( 4)                    /* configuration block */
#define SKEIN_BLK_TYPE_PERS     ( 8)                    /* personalization string */
#define SKEIN_BLK_TYPE_PK       (12)                    /* public key (for digital signature hashing) */
#define SKEIN_BLK_TYPE_KDF      (16)                    /* key identifier for KDF */
#define SKEIN_BLK_TYPE_NONCE    (20)                    /* nonce for PRNG */
#define SKEIN_BLK_TYPE_MSG      (48)                    /* message processing */
#define SKEIN_BLK_TYPE_OUT      (63)                    /* output stage */
#define SKEIN_BLK_TYPE_MASK     (63)                    /* bit field mask */

#define SKEIN_T1_BLK_TYPE(T)   (((uint64_t) (SKEIN_BLK_TYPE_##T)) << SKEIN_T1_POS_BLK_TYPE)
#define SKEIN_T1_BLK_TYPE_KEY   SKEIN_T1_BLK_TYPE(KEY)  /* key, for MAC and KDF */
#define SKEIN_T1_BLK_TYPE_CFG   SKEIN_T1_BLK_TYPE(CFG)  /* configuration block */
#define SKEIN_T1_BLK_TYPE_PERS  SKEIN_T1_BLK_TYPE(PERS) /* personalization string */
#define SKEIN_T1_BLK_TYPE_PK    SKEIN_T1_BLK_TYPE(PK)   /* public key (for digital signature hashing) */
#define SKEIN_T1_BLK_TYPE_KDF   SKEIN_T1_BLK_TYPE(KDF)  /* key identifier for KDF */
#define SKEIN_T1_BLK_TYPE_NONCE SKEIN_T1_BLK_TYPE(NONCE)/* nonce for PRNG */
#define SKEIN_T1_BLK_TYPE_MSG   SKEIN_T1_BLK_TYPE(MSG)  /* message processing */
#define SKEIN_T1_BLK_TYPE_OUT   SKEIN_T1_BLK_TYPE(OUT)  /* output stage */
#define SKEIN_T1_BLK_TYPE_MASK  SKEIN_T1_BLK_TYPE(MASK) /* field bit mask */

#define SKEIN_T1_BLK_TYPE_CFG_FINAL       (SKEIN_T1_BLK_TYPE_CFG | SKEIN_T1_FLAG_FINAL)
#define SKEIN_T1_BLK_TYPE_OUT_FINAL       (SKEIN_T1_BLK_TYPE_OUT | SKEIN_T1_FLAG_FINAL)

#define SKEIN_CFG_STR_LEN       (4*8)

/*
**   Skein macros for getting/setting tweak words, etc.
**   These are useful for partial input bytes, hash tree init/update, etc.
**/
#define Skein_Get_Tweak(ctxPtr,TWK_NUM)         ((ctxPtr)->h.T[TWK_NUM])
#define Skein_Set_Tweak(ctxPtr,TWK_NUM,tVal)    {(ctxPtr)->h.T[TWK_NUM] = (tVal);}

#define Skein_Get_T0(ctxPtr)    Skein_Get_Tweak(ctxPtr,0)
#define Skein_Get_T1(ctxPtr)    Skein_Get_Tweak(ctxPtr,1)
#define Skein_Set_T0(ctxPtr,T0) Skein_Set_Tweak(ctxPtr,0,T0)
#define Skein_Set_T1(ctxPtr,T1) Skein_Set_Tweak(ctxPtr,1,T1)

/* set both tweak words at once */
#define Skein_Set_T0_T1(ctxPtr,T0,T1)           \
    {                                           \
    Skein_Set_T0(ctxPtr,(T0));                  \
    Skein_Set_T1(ctxPtr,(T1));                  \
    }

/* set up for starting with a new type: h.T[0]=0; h.T[1] = NEW_TYPE; h.bCnt=0; */
#define Skein_Start_New_Type(ctxPtr,BLK_TYPE)   \
    { Skein_Set_T0_T1(ctxPtr,0,SKEIN_T1_FLAG_FIRST | SKEIN_T1_BLK_TYPE_##BLK_TYPE); (ctxPtr)->h.bCnt=0; }

#define Skein_Clear_First_Flag(hdr)      { (hdr).T[1] &= ~SKEIN_T1_FLAG_FIRST;       }
#define Skein_Set_Bit_Pad_Flag(hdr)      { (hdr).T[1] |=  SKEIN_T1_FLAG_BIT_PAD;     }


#ifndef SKEIN_ERR_CHECK        /* run-time checks (e.g., bad params, uninitialized context)? */
#define Skein_Assert(x,retCode)/* default: ignore all Asserts, for performance */
#define Skein_assert(x)
#elif   defined(SKEIN_ASSERT)
#include <assert.h>
#define Skein_Assert(x,retCode) assert(x)
#define Skein_assert(x)         assert(x)
#else
#include <assert.h>
#define Skein_Assert(x,retCode) { if (!(x)) return retCode; } /*  caller  error */
#define Skein_assert(x)         assert(x)                     /* internal error */
#endif


enum
{
	SKEIN_SUCCESS         =      0,          /* return codes from Skein calls */
	SKEIN_FAIL            =      1,
	SKEIN_BAD_HASHLEN     =      2
};

#define  SKEIN_MODIFIER_WORDS  ( 2)          /* number of modifier (tweak) words */
#define  SKEIN_256_STATE_WORDS ( 4)
#define  SKEIN_256_STATE_BYTES ( 8*SKEIN_256_STATE_WORDS)
#define  SKEIN_256_BLOCK_BYTES ( 8*SKEIN_256_STATE_WORDS)

#define BLK_BITS    (WCNT*64)

#define SKEIN_RND_SPECIAL       (1000u)
#define SKEIN_RND_KEY_INITIAL   (SKEIN_RND_SPECIAL+0u)
#define SKEIN_RND_KEY_INJECT    (SKEIN_RND_SPECIAL+1u)
#define SKEIN_RND_FEED_FWD      (SKEIN_RND_SPECIAL+2u)

/* macro to perform a key injection (same for all block sizes) */
#define InjectKey(r)                                                \
    for (i=0;i < WCNT;i++)                                          \
         X[i] += ks[((r)+i) % (WCNT+1)];                            \
    X[WCNT-3] += ts[((r)+0) % 3];                                   \
    X[WCNT-2] += ts[((r)+1) % 3];                                   \
    X[WCNT-1] += (r);                    /* avoid slide attacks */

typedef struct
{
	size_t  hashBitLen;                      /* size of hash result, in bits */
	size_t  bCnt;                            /* current byte count in buffer b[] */
	uint64_t  T[SKEIN_MODIFIER_WORDS];         /* tweak words: T[0]=byte cnt, T[1]=flags */
} Skein_Ctxt_Hdr_t;


typedef struct                               /*  256-bit Skein hash context structure */
{
	Skein_Ctxt_Hdr_t h;                      /* common header context variables */
	uint64_t  X[SKEIN_256_STATE_WORDS];        /* chaining variables */
	uint8_t  b[SKEIN_256_BLOCK_BYTES];        /* partial block buffer (8-byte aligned) */
} Skein_256_Ctxt_t;


typedef enum
{
	SUCCESS     = SKEIN_SUCCESS,
	FAIL        = SKEIN_FAIL,
	BAD_HASHLEN = SKEIN_BAD_HASHLEN
}
HashReturn;

typedef struct
{
	unsigned int  statebits;                      /* 256, 512, or 1024 */
	union
	{
		Skein_Ctxt_Hdr_t h;                 /* common header "overlay" */
		Skein_256_Ctxt_t ctx_256;
	} u;
}
hashState;



// Implements a hash function:
// Derived from the public domain Skein reference implementation
// (credits to the Skein team: see http://www.skein-hash.info/ )
// DO NOT COPY THIS CODE AND USE IT FOR CRYPTOGRAPHIC PURPOSE
// THIS VERSION HAS BEEN SIMPLIFIED AND IS PROBABLY INSECURE
// It is meant to prevent cheating by editing budget, levels or config
class Hash
{
private:
	const size_t SKEIN_256_ROUNDS_TOTAL = 72; /* number of rounds for the different block sizes */
	// Skein block function constants (shared across Ref and Opt code)
	enum
	{
		/* Skein_256 round rotation constants */
		R_256_0_0= 5, R_256_0_1=56,
		R_256_1_0=36, R_256_1_1=28,
		R_256_2_0=13, R_256_2_1=46,
		R_256_3_0=58, R_256_3_1=44,
		R_256_4_0=26, R_256_4_1=20,
		R_256_5_0=53, R_256_5_1=35,
		R_256_6_0=11, R_256_6_1=42,
		R_256_7_0=59, R_256_7_1=50,

		/* Skein_512 round rotation constants */
		R_512_0_0=38, R_512_0_1=30, R_512_0_2=50, R_512_0_3=53,
		R_512_1_0=48, R_512_1_1=20, R_512_1_2=43, R_512_1_3=31,
		R_512_2_0=34, R_512_2_1=14, R_512_2_2=15, R_512_2_3=27,
		R_512_3_0=26, R_512_3_1=12, R_512_3_2=58, R_512_3_3= 7,
		R_512_4_0=33, R_512_4_1=49, R_512_4_2= 8, R_512_4_3=42,
		R_512_5_0=39, R_512_5_1=27, R_512_5_2=41, R_512_5_3=14,
		R_512_6_0=29, R_512_6_1=26, R_512_6_2=11, R_512_6_3= 9,
		R_512_7_0=33, R_512_7_1=51, R_512_7_2=39, R_512_7_3=35,

		/* Skein1024 round rotation constants */
		R1024_0_0=55, R1024_0_1=43, R1024_0_2=37, R1024_0_3=40, R1024_0_4=16, R1024_0_5=22, R1024_0_6=38, R1024_0_7=12,
		R1024_1_0=25, R1024_1_1=25, R1024_1_2=46, R1024_1_3=13, R1024_1_4=14, R1024_1_5=13, R1024_1_6=52, R1024_1_7=57,
		R1024_2_0=33, R1024_2_1= 8, R1024_2_2=18, R1024_2_3=57, R1024_2_4=21, R1024_2_5=12, R1024_2_6=32, R1024_2_7=54,
		R1024_3_0=34, R1024_3_1=43, R1024_3_2=25, R1024_3_3=60, R1024_3_4=44, R1024_3_5= 9, R1024_3_6=59, R1024_3_7=34,
		R1024_4_0=28, R1024_4_1= 7, R1024_4_2=47, R1024_4_3=48, R1024_4_4=51, R1024_4_5= 9, R1024_4_6=35, R1024_4_7=41,
		R1024_5_0=17, R1024_5_1= 6, R1024_5_2=18, R1024_5_3=25, R1024_5_4=43, R1024_5_5=42, R1024_5_6=40, R1024_5_7=15,
		R1024_6_0=58, R1024_6_1= 7, R1024_6_2=32, R1024_6_3=45, R1024_6_4=19, R1024_6_5=18, R1024_6_6= 2, R1024_6_7=56,
		R1024_7_0=47, R1024_7_1=49, R1024_7_2=27, R1024_7_3=58, R1024_7_4=37, R1024_7_5=48, R1024_7_6=53, R1024_7_7=56
	};

	int Skein_256_Init(Skein_256_Ctxt_t *ctx, size_t hashBitLen);
	int Skein_256_InitExt(Skein_256_Ctxt_t *ctx,size_t hashBitLen,uint64_t treeInfo, const uint8_t *key, size_t keyBytes);
	int Skein_256_Update(Skein_256_Ctxt_t *ctx, const uint8_t *msg, size_t msgByteCnt);
	int Skein_256_Final(Skein_256_Ctxt_t *ctx, uint8_t *hashVal);
	void Skein_256_Process_Block(Skein_256_Ctxt_t *ctx,const uint8_t *blkPtr,size_t blkCnt,size_t byteCntAdd);


	void Skein_Put64_LSB_First(uint8_t *dst,const uint64_t *src,size_t bCnt);
	void Skein_Get64_LSB_First(uint64_t *dst,const uint8_t *src,size_t wCnt);
	uint64_t RotL_64(uint64_t x,unsigned int N);

public:
	Hash();
	~Hash();

	HashReturn Init(hashState *state, int hashbitlen);
	HashReturn Update(hashState *state, const uint8_t *data, size_t databitlen);
	HashReturn Final(hashState *state, uint8_t *hashval);

	/* all-in-one call hash function */
	//
	HashReturn Hash_256(int hashbitlen, const uint8_t *data, size_t databitlen, uint8_t *hashval);
};

#endif

