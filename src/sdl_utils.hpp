//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_SDL_UTILS_H
#define HEADER_SDL_UTILS_H

namespace SDL_utils{
	bool same_surface_gemometry(const SDL_Surface* s1, const SDL_Surface* s2);

	// make a color gradient mapping of two surfaces (inplace)
	// return: surface s_color2 is changed
	void create_color_gradient(const SDL_Surface* s_color1, SDL_Surface* s_color2);

	// returns optimal font_size or something < 0 on failure
	int test_for_font_size(const std::string& font_filename, const std::string& text, int w, int h, int font_size);

} // end namespace SDL_utils
#endif
