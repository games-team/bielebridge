//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <iostream>
#include <cmath>
#include <cfloat>

#include "vector2d.hpp"
#include "spring.hpp"
#include "particle.hpp"
#include "math_utils.hpp"
#include "error.hpp"

// returns true if point "p" is blow line "ab"
// line segment must not be vertical
bool Math_utils::point_below_line_segment(const float_32 px, const float_32 py, const float_32 ax, const float_32 ay, const float_32 bx, const float_32 by)
{
#ifdef DEBUG
	if( ! (ax <= px && px <= bx) ){
		// bad function call: Point is not between line ends
		ERROR_PRINT("bad function call: point [%f,%f] is not between line ends ax [%f,%f] and bx [%f,%f] (ax <= px && px <= bx)\n",
				px, py, ax, ay, bx, by);
	}
#endif
	if( (by-ay)*(px-ax)/(bx-ax) + ay > py)
		return true;
	else
		return false;
}

// returns true if point p is below line ab or (if ax == bx) point is left
bool Math_utils::point_below_line(const Vector2d& p, const Vector2d& a, const Vector2d& b)
{
	if( a.x == b.x ){
		if(a.y != b.y){
			if( p.x < a.x)
				return true;
			else
				return false;
		}
		else{
			ERROR_PRINT("bad function call: a and b are identical [%f,%f]. No line to contstruct.\n", a.x, a.y);
			return false;
		}
	}

	if( (b.y-a.y)*(p.x-a.x)/(b.x-a.x) + a.y > p.y)
		return true;
	else
		return false;
}


// Point in polygon Algorithm from Wm. Randolph Franklin, see
// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
// Copyright (c) 1970-2003, Wm. Randolph Franklin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
//  1.) Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimers.
//  2.) Redistributions in binary form must reproduce the above copyright
//  notice in the documentation and/or other materials provided with the
//  distribution.
//  3.) The name of W. Randolph Franklin may not be used to endorse or promote
//  products derived from this Software without specific prior written
//  permission.
bool Math_utils::point_in_polygon(const Vector2d& test_p, const Vector2d* vertices, const int vertices_num)
{
	int i, j;
	bool c = false;
	for (i = 0, j = vertices_num-1; i < vertices_num; j = i++)
	{
		if ( ((vertices[i].y > test_p.y) != (vertices[j].y > test_p.y)) &&
				(test_p.x < (vertices[j].x - vertices[i].x) * (test_p.y - vertices[i].y) / (vertices[j].y - vertices[i].y) + vertices[i].x) )
		{
			c = !c;
		}
	}
	return c;
}


float_32 Math_utils::point_min_distsq_to_points(const Vector2d& test_p, const Vector2d* vertices, const int vertices_num)
{
  	float_32 min_dist_sq = FLT_MAX;
  	float_32 cur_dist_sq = 0.0f;
	int i;
	for (i = 0; i < vertices_num; i++)
	{
		cur_dist_sq = Vector2d::distance_square(test_p, vertices[i]);
		if ( cur_dist_sq < min_dist_sq)
		{
			min_dist_sq = cur_dist_sq;
		}
	}
	return min_dist_sq;
}

// calculated by average
Vector2d Math_utils::center_of_polygon(const Vector2d* vertices, const int vertices_num)
{
	Vector2d avg_center = vertices[0];
	for (int i = 1; i < vertices_num; i++)
	{
		avg_center += vertices[i];
	}
	avg_center /= (float_32) vertices_num;
	return avg_center;
}

int Math_utils::get_nearest_spring(Spring** springs, const int springs_num, Vector2d point)
{
	int nearest_index = 0;
	float_32 nearest_dist = Math_utils::get_distance_square_point_line_segment(point, springs[0]->particles.first->pos, springs[0]->particles.second->pos);
	for (int i = 1; i < springs_num; i++)
	{
		float_32 dist = Math_utils::get_distance_square_point_line_segment(point, springs[i]->particles.first->pos, springs[i]->particles.second->pos);
		if( dist < nearest_dist)
		{
			nearest_index = i;
			nearest_dist = dist;
		}
	}
	return nearest_index;
}

// old and rusted!! to DELETE
int Math_utils::point_in_convex_polygon(const Vector2d* vertices, const int vertices_num, const Vector2d& testp)
{
	// create normal to edge p1 p2 (it automatically points to the outside of the polygon)
	bool return0 = false;
	for( int i=0 ; i<vertices_num && !return0 ; ++i )
	{
		Vector2d normal( vertices[i].y - vertices[(i+1)%vertices_num].y, vertices[(i+1)%vertices_num].x - vertices[i].x );
		if( normal.dot(testp - vertices[i]) > 0 ) return0 = true;
	}
	if(return0) return 0;

	// TODO: is it faster to put the calculated normals into an array?
	// point inside; calculate nearest line to the testpoint
	float_32 mindist = 99999.0f;
	int mindist_line = 0;
	for( int i=0 ; i<vertices_num; ++i )
	{
		Vector2d normal( vertices[i].y - vertices[(i+1)%vertices_num].y, vertices[(i+1)%vertices_num].x - vertices[i].x );
		normal.normalize();
		if( fabs( normal.dot(testp - vertices[i]) ) < mindist)
		{
			mindist = (float_32) fabs(double( normal.dot(testp - vertices[i]) ));
			mindist_line = i+1;
		}
	}
	// if the collision happens near to a corner this algorithm fails
	// it retruns an upper line while ground is crossin two times an lower (less y) line in the polygon
	return mindist_line;
}


int Math_utils::ground_point_in_polygon(const Vector2d* vertices, const int vertices_num, const Vector2d& testp)
{
	// create normal to edge p1 p2 (it automatically points to the outside of the polygon)
	bool return0 = false;
	for( int i=0 ; i<vertices_num && !return0 ; ++i )
	{
		Vector2d normal( vertices[i].y - vertices[(i+1)%vertices_num].y, vertices[(i+1)%vertices_num].x - vertices[i].x );
		if( normal.dot(testp - vertices[i]) > 0 ) return0 = true;
	}
	// return 0 if point is outside
	if(return0) return 0;

	// calculate crossing line

	// take a point 200 x below testpoint (for this game certainly outside the polygon)
	// and test which line cross
	int collision_line = 0;
	Vector2d point_below_outside(testp.x, testp.y - 200.0f);
	Vector2d unused_cross_point(0.0f, 0.0f);
	for( int i=0 ; i<vertices_num && !collision_line; ++i )
	{
		if(	Math_utils::lines_cross(vertices[i], vertices[(i+1)%vertices_num] - vertices[i], testp, point_below_outside - testp, unused_cross_point ))
			collision_line = i+1;
	}
#ifdef DEBUG
	if(collision_line == 0)
		ERROR_PRINT("%s\n","Math Error: collision_line == 0 but point is inside polygon");
#endif
	return collision_line;
}

inline bool Math_utils::point_in_rect(const Vector2d& min, const Vector2d& max, const Vector2d& p)
{
	if( p.x < min.x || p.x > max.x ) return false;
	if( p.y < min.y || p.y > max.y ) return false;
	return true;
}

inline bool Math_utils::points_in_rect(const Vector2d& min, const Vector2d& max, const Vector2d& p, const Vector2d& q)
{
	if( p.x > min.x && p.x < max.x &&  p.y > min.y && p.y < max.y ) return true;
	if( q.x > min.x && q.x < max.x &&  q.y > min.y && q.y < max.y ) return true;
	return false;
}

// not line segments!
bool Math_utils::lines_cross_points(const Vector2d& p1, const Vector2d& p2, const Vector2d& r1, const Vector2d& r2, Vector2d& interceptP)
{
	// calculate point vector form
	return lines_cross(p1, p2 - p1, r1, r2 - r1, interceptP);
}

// not line segments! point vector form
bool Math_utils::lines_cross(const Vector2d& p1, const Vector2d& v1, const Vector2d& p2, const Vector2d& v2, Vector2d& interceptP)
{
	// Die Determinante D berechnen
	const float_32 D = v1.y * v2.x - v1.x * v2.y;

	// Wenn D null ist, sind die Linien parallel.
	if(D < 0.00001f && D > -0.00001f) return false;

	// Determinante Ds berechnen
	const float_32 Ds = (p2.y - p1.y) * v2.x - (p2.x - p1.x) * v2.y;
	// s = Ds / D berechnen und prüfen, ob s in den Grenzen liegt
	const float_32 s = Ds / D;
	if(s < 0.0f || s > 1.0f) return false;

	// Jetzt berechnen wir Dt und t.
	const float_32 Dt = (p2.y - p1.y) * v1.x - (p2.x - p1.x) * v1.y;

	const float_32 t = Dt / D;
	if(t < 0.0f || t > 1.0f) return false;

	interceptP = p1 + v1 * s;
	return true;
}

Particle* Math_utils::springs_have_common_particle(const Spring* s1, const Spring* s2)
{
	if( s1->particles.first == s2->particles.first || s1->particles.first == s2->particles.second )
		return s1->particles.first;
	else if( s1->particles.second == s2->particles.first || s1->particles.second == s2->particles.second )
		return s1->particles.second;
	else
		return nullptr;
}

int Math_utils::move_particle_out_of_polygon(const Vector2d* vertices, const int vertices_num, Particle* p)
{
	// find the outline the particle crossed
	const float_32 searchlen = 2.0; // constant to tune
	Vector2d interceptP(0.0, 0.0);
	int i=0;
	for( ; i<vertices_num ; ++i )
	{
		// lines_cross( polygonx, polygonx+1, particlepos, prevparticlepos);
		if( Math_utils::lines_cross( vertices[i],vertices[(i+1)%vertices_num], p->pos, p->pos - (p->velocity * searchlen ), interceptP ) )
			break;
	}
	const Vector2d null(0.0, 0.0);
	if (fabs(interceptP.x) + fabs(interceptP.y) < 0.0001f )
	{
		std::cerr<<"no interception point found"<<std::endl;
		return -1;
	}
	else
	{
		// to get the point a bit out of the polygon and not exactly on the outline
		const float_32 displace = 0.1f;
		p->pos = interceptP - (p->velocity * displace);
		return i;
	}
}

// return normal that points right
Vector2d Math_utils::get_normal(const Vector2d& v)
{
	return Vector2d(v.y, -v.x);
}

Vector2d Math_utils::get_normal_projection(const Vector2d& vectortoproject, const Vector2d& projectionbase)
{
	Vector2d projectionbase_norm(projectionbase);
	projectionbase_norm.normalize();

	return Vector2d(projectionbase_norm * (vectortoproject.dot(projectionbase_norm)));
}

float_32 Math_utils::get_distance_point_line(const Vector2d& point, const Vector2d& pointline, const Vector2d& vectorline)
{
	Vector2d normal_projection = Math_utils::get_normal_projection(point - pointline, vectorline);
	return (point - pointline - normal_projection).length();
}

float_32 Math_utils::get_distance_point_line_segment(const Vector2d& point, const Vector2d& seg_a, const Vector2d& seg_b)
{
  // Return minimum distance between line segment vw and point p
  const float_32 l2 = Vector2d::distance_square(seg_a, seg_b);  // i.e. |w-v|^2 -  avoid a sqrt
  if (l2 == 0.0) return Vector2d::distance(point, seg_a);   // v == w case

  // Consider the line extending the segment, parameterized as v + t (w - v).
  // We find projection of point p onto the line.
  // It falls where t = [(p-v) . (w-v)] / |w-v|^2
  const float_32 t = Vector2d::dot(point - seg_a, seg_b - seg_a) / l2;
  if (t < 0.0) {
	  return Vector2d::distance(point, seg_a);       // Beyond the 'v' end of the segment
  }
  else {
	  if (t > 1.0)
		  return Vector2d::distance(point, seg_b);  // Beyond the 'w' end of the segment
  }

  const Vector2d projection = seg_a + (seg_b - seg_a) * t;  // Projection falls on the segment
  return Vector2d::distance(point, projection);
}

float_32 Math_utils::get_distance_square_point_line_segment(const Vector2d& point, const Vector2d& seg_a, const Vector2d& seg_b)
{
  // Return minimum distance between line segment vw and point p
  const float_32 l2 = Vector2d::distance_square(seg_a, seg_b);  // i.e. |w-v|^2 -  avoid a sqrt
  if (l2 == 0.0) return Vector2d::distance_square(point, seg_a);   // v == w case

  // Consider the line extending the segment, parameterized as v + t (w - v).
  // We find projection of point p onto the line.
  // It falls where t = [(p-v) . (w-v)] / |w-v|^2
  const float_32 t = Vector2d::dot(point - seg_a, seg_b - seg_a) / l2;
  if (t < 0.0) {
	  return Vector2d::distance_square(point, seg_a);       // Beyond the 'v' end of the segment
  }
  else {
	  if (t > 1.0)
		  return Vector2d::distance_square(point, seg_b);  // Beyond the 'w' end of the segment
  }

  const Vector2d projection = seg_a + (seg_b - seg_a) * t;  // Projection falls on the segment
  return Vector2d::distance_square(point, projection);
}

void Math_utils::velocity_projection(Particle* p, const Vector2d& v)
{
	//Vector2d normale(Math_utils::get_normal(v));
	//p->velocity -= Math_utils::get_normal_projection(p->velocity, normale);
	p->velocity = Math_utils::get_normal_projection(p->velocity, v);
}

Particle* Math_utils::get_nearer_particle(const Vector2d& point, const Spring* s)
{
	if(Vector2d::distance_square(point, s->particles.first->pos) < Vector2d::distance_square(point, s->particles.second->pos))
		return s->particles.first;
	else
		return s->particles.second;
}

Vector2d Math_utils::get_nearer_point(const Vector2d& ref, const Vector2d& a, const Vector2d& b)
{
	if(Vector2d::distance_square(ref, a) < Vector2d::distance_square(ref, b))
		return a;
	else
		return b;
}

void Math_utils::apply_impulse(Spring* deck, Particle* train_point)
{
	Vector2d delta_velocity = train_point->velocity;
	Math_utils::velocity_projection(train_point, deck->particles.second->pos - deck->particles.first->pos);
	delta_velocity -= train_point->velocity;

	Vector2d impulse = delta_velocity * (1.0f / train_point->inv_mass) * 1.0f ;
	// FIXME: this is bad physics; only for testing
	if(!deck->particles.first->fixed)  deck->particles.first->velocity  += impulse ;
	if(!deck->particles.second->fixed) deck->particles.second->velocity += impulse ;



	// FIXME real physics? (doesnt work)
//	float_32 position = (deck->particles.first->pos - train_point->pos).length() / (deck->particles.second->pos - train_point->pos).length();
//	if(!deck->particles.first->fixed)  deck->particles.first->velocity  += impulse * position;
//	if(!deck->particles.second->fixed) deck->particles.second->velocity += impulse * (1/position);
}

/// simplified version of line segment intersection test with orientation
/// see
/// http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// @return 0: p, q and r are colinear
// @return 1: Clockwise
// @return 2: Counterclockwise
uint_fast8_t Math_utils::orientation(const Vector2d& p, const Vector2d& q, const Vector2d& r)
{
    // See 10th slides from following link for derivation of the formula
    // http://www.dcs.gla.ac.uk/~pat/52233/slides/Geometry1x1.pdf
    float_32 val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);

    if (val == 0) return 0;  // colinear

    return (val > 0)? 1: 2; // clock or counterclock wise
}

// This function returns true if line segment 's1s2'
// and 't1t2' intersect.
bool Math_utils::line_segments_cross(const Vector2d& s1, const Vector2d& s2, const Vector2d& t1, const Vector2d& t2)
{
    // Find the four orientations needed for general and
    // special cases
    int o1 = Math_utils::orientation(s1, s2, t1);
    int o2 = Math_utils::orientation(s1, s2, t2);
    int o3 = Math_utils::orientation(t1, t2, s1);
    int o4 = Math_utils::orientation(t1, t2, s2);

    // General case
    if (o1 != o2 && o3 != o4)
        return true;

	// There are special cases with points exactly on the other
	// line segment ... this code ignore this.

    return false; // Doesn't fall in any of the above cases
}

/* returns penetration depth of p in polygon with vertices "* and G, (H)"
 * to calculate this o and q are also needed; point_vertices_num is always 3;
 * Center of polygon (C) defines the outside of "op" and "pq";
 * (this function uses only one sqrt)
 *                                                q
 *                                                |
 *     *------------*            *     *----------x-H
 *     |            |            *     |          | |
 *     |            |            *     |          | |
 *     |     C      |            *     |     C    | |
 *     |      p-----x---q        *     |          p |
 *     |     /      |           or     |         /  |
 *     *----x-------G            *     *--------x---G
 *         /                     *             /
 *        /                      *            /
 *       o                       *           o
 * this algorithm will fail, if penetration is so deeper than center of polygon (this should never happen)
 */
float_32 Math_utils::pen_depth_point_in_polygon(const Vector2d* poly_vertices, const int poly_vertices_num, const Vector2d* point_vertices)
{
	// test if p is realy inside polygon
	if(! Math_utils::point_in_polygon(point_vertices[1], poly_vertices, poly_vertices_num)){
		ERROR_PRINT("point [%f, %f] is not inside polygon\n", point_vertices[1].x, point_vertices[1].y);
		return 0;
	}

	Vector2d center = center_of_polygon(poly_vertices, poly_vertices_num);
	// test if penetration is too deep
	// therefor get min distance from C to outline
	float_32 nearest_dist_sq = Math_utils::get_distance_square_point_line_segment(center, poly_vertices[poly_vertices_num-1] , poly_vertices[0]);
	for (int i = 1; i < poly_vertices_num; i++)
	{
		float_32 dist_sq = Math_utils::get_distance_square_point_line_segment(center, poly_vertices[i-1], poly_vertices[i]);
		if( dist_sq < nearest_dist_sq)
			nearest_dist_sq = dist_sq;
	}
	float_32 nearest_dist = std::sqrt(nearest_dist_sq);
	if( Vector2d::distance(center, point_vertices[1]) < nearest_dist / 2.0f ){
		ERROR_PRINT("%s\n", "penetration ist too deep; it remains unclear what is inside/outside of deck line...");
		// do nothing
		return 0;
	}

	// STEP 1: get min dest p to polygon segments
	float_32 min_pen_depth_sq = FLT_MAX; // set to max
	for (int i = 0; i < poly_vertices_num; i++)
	{
		float_32 cur_pen_depth_sq = Math_utils::get_distance_square_point_line_segment(point_vertices[1], poly_vertices[i], poly_vertices[(i+1) % poly_vertices_num]);
		// if smaller, set new minimum
		if( cur_pen_depth_sq < min_pen_depth_sq)
			min_pen_depth_sq = cur_pen_depth_sq;
	}

	// STEP 2: get min dest G, H .... to "op" and "pq"
	float_32 max_pen_depth_sq = 0; // maximum of all points G, H, ...

	// finding G, H, ....
	// G is a vertex with a crossing "CG" and ("op" or "pq")
	for (int p = 0; p < poly_vertices_num; p++)
	{
		if(line_segments_cross(center, poly_vertices[p], point_vertices[0], point_vertices[1]) ||
			line_segments_cross(center, poly_vertices[p], point_vertices[1], point_vertices[2]))
		{
			// found G, H, ... it is poly_vertices[p]
			float_32 min_pen_depth_per_point_sq = FLT_MAX; // minimum distance for each point
			float_32 cur_pen_depth_sq1 = Math_utils::get_distance_square_point_line_segment(poly_vertices[p], point_vertices[0], point_vertices[1]);
			// if smaller, set new minimum
			if( cur_pen_depth_sq1 < min_pen_depth_per_point_sq)
				min_pen_depth_per_point_sq = cur_pen_depth_sq1;

			cur_pen_depth_sq1 = Math_utils::get_distance_square_point_line_segment(poly_vertices[p], point_vertices[1], point_vertices[2]);
			// if smaller, set new minimum
			if( cur_pen_depth_sq1 < min_pen_depth_per_point_sq)
				min_pen_depth_per_point_sq = cur_pen_depth_sq1;

			// if current minumum ist larger than maximum of all points, set new max
			if( min_pen_depth_per_point_sq > max_pen_depth_sq)
				max_pen_depth_sq = min_pen_depth_per_point_sq;
		}
	}

	// STEP 3: return maximum of nearest distance and do sqrt
	return std::sqrt(fmaxf(min_pen_depth_sq, max_pen_depth_sq));
}

// force_pos is on line [target_1_pos, target_2_pos]
// force_vec is normal to [target_1_pos, target_2_pos]
void Math_utils::normal_force_split(const Vector2d& force_vec, const Vector2d& force_pos,
								const Vector2d& target_1_pos, const Vector2d& target_2_pos,
								Vector2d& result_at_pos_1, Vector2d& result_at_pos_2)
{
#ifdef DEBUG_MATH
	// some checks
	Vector2d mid_point = target_1_pos + (target_2_pos - target_1_pos) * 0.5f;
	// check if
	if(Vector2d::distance(mid_point, force_pos) > Vector2d::distance(target_1_pos, target_2_pos) * 0.5)
		ERROR_PRINT("%s\n", "force_pos not inside circle between target positions");
	if(get_distance_point_line(force_pos, target_1_pos, (target_2_pos - target_1_pos)) > 0.1f)
		ERROR_PRINT("%s\n", "force_pos is not on target line");
#endif

	float_32 dist_force_to_t1 = Vector2d::distance(force_pos, target_1_pos);
	float_32 dist_force_to_t2 = Vector2d::distance(force_pos, target_2_pos);

	float_32 sum_dist = dist_force_to_t1 + dist_force_to_t2;

	result_at_pos_1 = force_vec * (dist_force_to_t2 / sum_dist);
	result_at_pos_2 = force_vec * (dist_force_to_t1 / sum_dist);
}

bool Math_utils::line_segment_polygon_cross(const Vector2d& s1, const Vector2d& s2, const Vector2d* vertices, const int vertices_num)
{
	int i, j;
	bool cross = false;
	for (i = 0, j = vertices_num-1; i < vertices_num && !cross; j = i++)
	{
		if(Math_utils::line_segments_cross(vertices[i], vertices[j], s1, s2))
			cross = true;
	}
	return cross;
}

/* retruns fast inverse square root
* https://en.wikipedia.org/wiki/Fast_inverse_square_root
* Code from paper: FAST INVERSE SQUARE ROOT by CHRIS LOMONT
*
* this function only works with 32 Bit float
float Math_utils::InvSqrt(float x)
{
	float xhalf = 0.5f*x;
	int i = *(int*)&x;
	i = 0x5f375a86- (i>>1);
	x = *(float*)&i;
	x = x*(1.5f-xhalf*x*x);
	return x;
}
*/


