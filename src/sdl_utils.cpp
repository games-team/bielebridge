//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_ttf.h>
#include <cmath>

#include "settings.hpp"
#include "error.hpp"
#include "sdl_utils.hpp"

bool SDL_utils::same_surface_gemometry(const SDL_Surface* s1, const SDL_Surface* s2)
{
	if( s1->format->BytesPerPixel == s2->format->BytesPerPixel &&
		s1->w == s2->w &&
		s1->h == s2->h &&
		s1->pitch == s2->pitch)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void SDL_utils::create_color_gradient(const SDL_Surface* s_color1, SDL_Surface* s_color2)
{
	// both surfaces must have same geometry
	if( s_color1->format->BytesPerPixel != s_color2->format->BytesPerPixel ||
		s_color1->w != s_color2->w ||
		s_color1->h != s_color2->h ||
		s_color1->pitch != s_color2->pitch)
	{
		ERROR_PRINT("Surfaces have different geometry:\n"
				"  s_color1: %d,%d,%d,%d (BytesPerPixel,w,h,pitch)\n"
				"  s_color2: %d,%d,%d,%d (BytesPerPixel,w,h,pitch)\n",
				s_color1->format->BytesPerPixel, s_color1->w, s_color1->h, s_color1->pitch,
				s_color2->format->BytesPerPixel, s_color2->w, s_color2->h, s_color2->pitch);
		// do nothing
		return;
	}

	int number_pixel_bytes = s_color1->format->BytesPerPixel * s_color1->w * s_color1->h;
	float blend_level = 0.0f;
	for(int i = 0; i < number_pixel_bytes; i++)
	{
		// this is a hack to create a color gradient from two single color surfaces
		// TODO: improve this
		blend_level = (float) i / (float) number_pixel_bytes;
		uint8_t* s1 = (uint8_t*) s_color1->pixels;
		uint8_t* s2 = (uint8_t*) s_color2->pixels;
		s2[i] =(uint8_t) (((float) s1[i]) * (1.0f - blend_level) + ((float) s2[i]) * blend_level);
	}
}

// returns optimal font_size or something < 0 on failure
int SDL_utils::test_for_font_size(const std::string& font_filename, const std::string& text, int w, int h, int font_size)
{
//	if(text.compare("Level 1") == 0)
//		INFO_PRINT("test font size: %d\n",font_size);

	int surface_w, surface_h;

	TTF_Font *font = TTF_OpenFont(font_filename.c_str(), font_size);
	if( font == nullptr)
	{
		SDLERROR_PRINT("Couldn't open font file: %s\n", font_filename.c_str());
		return -1;
	}
	else
	{
		SDL_Color test_color = {0xff, 0xff, 0xff, 0xff};
		SDL_Surface* tmp_surface = TTF_RenderUTF8_Blended( font, text.c_str(), test_color);
		if(tmp_surface == nullptr)
		{
			ERROR_PRINT("Couldn't render to surface this text: '%s'\n", text.c_str());
			TTF_CloseFont(font);
			return -1;
		}
		surface_w = tmp_surface->w;
		surface_h = tmp_surface->h;
		SDL_FreeSurface(tmp_surface);
	}
	TTF_CloseFont(font);

	const float margin = Settings::singleton()->text_to_box_margin;
	// test if surface is too big
	if( float(surface_w) * margin > float(w) || float(surface_h) * margin > float(h) )
	{
		double ratio = fmax(surface_w / w, surface_h / h);
		if( ratio > 1.00f)
			return SDL_utils::test_for_font_size(font_filename, text, w, h, int( std::round(double(font_size) / ratio)));
		else
			return SDL_utils::test_for_font_size(font_filename, text, w, h, font_size - 1);
	}
	else
	{
		return font_size;
	}
}
