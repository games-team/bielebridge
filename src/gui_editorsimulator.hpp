//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_EDITORSIMULATOR_H
#define HEADER_EDITORSIMULATOR_H

#include <vector>
#include "settings.hpp"

/// base class for editor and simulator doing zoom/translate
class Spring;
class Particle;
class Level;
class IDraw_Move_Zoom;

class GUI_Editorsimulator
{
protected:
	IDraw_Move_Zoom* sdl;
	Level* level;
public:
	typedef std::vector<Spring*>::iterator Spring_iter;
	typedef std::vector<Particle*>::iterator Particle_iter;

	GUI_Editorsimulator(){};
	virtual ~GUI_Editorsimulator(){};

	virtual void event_keyboard(const char* utf8_key);
	virtual void event_mouse_wheel(int32_t y);
};

#endif

