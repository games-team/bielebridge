//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_GUI_EDITOR_UNDOMANAGER_H
#define HEADER_GUI_EDITOR_UNDOMANAGER_H

#include <list>

class Spring;
class Particle;
class Level;

class GUI_Editor_Undomanager
{
private:
	// maybe list is not the best choice
	std::list<Level*> undo_level_list;

public:
	GUI_Editor_Undomanager();
	~GUI_Editor_Undomanager();
	void save_current_state(const Level* level);
	Level* undo(Level* level);
	Level* redo(Level* level);
};

#endif
