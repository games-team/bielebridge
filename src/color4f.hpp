//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//  Copyright (C) 2002 Ingo Ruhnke <grumbel@gmx.de>
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_COLOR3F_H
#define HEADER_COLOR3F_H

#include "settings.hpp"

class Color4f
{
public:
	float r;
	float g;
	float b;
	float a;

	Color4f() : r(0), g(0), b(0), a(1.0) {}

	Color4f(float r_, float g_, float b_, float a_) : r (r_), g (g_), b(b_), a(a_){}

	Color4f(const Color4f& other) :r(other.r), g(other.g), b(other.b), a(other.a){}
};

#endif

