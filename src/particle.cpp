//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//  Copyright (C) 2002 Ingo Ruhnke <grumbel@gmx.de>
//
//  This file is part of bielebridge but was at least
//  partially developed for construo by Ingo Ruhnke.
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.


#include <iostream>
#include <cmath>
#include "vector2d.hpp"
#include "math_utils.hpp"
#include "particle.hpp"
#include "error.hpp"

Particle::Particle()
  : id (-1),
    pos (0,0),
    velocity (0,0),
    mass (1),
    inv_mass (1),
    fixed (false),
    used_in_train (false),
    totale_force (0,0),
    spring_links (0)
{}

Particle::Particle (const int i, const Vector2d& arg_pos, float_32 m, bool f, bool used_in_train_)
  : id (i),
    pos (arg_pos),
    velocity (0,0),
    mass (m),
    inv_mass (1.0f/m),
    fixed (f),
    used_in_train (used_in_train_),
    totale_force (0,0),
    spring_links (0)
{}

Particle::Particle (const Particle& p)
  : id (p.id),
    pos (p.pos),
    velocity (p.velocity),
    mass (p.mass),
    inv_mass (p.inv_mass),
    fixed (p.fixed),
    used_in_train (p.used_in_train),
    totale_force (0,0),
    spring_links (0)
{}

void Particle::update (const float_32 delta_t, const float_32 water_height)
{
	if (!fixed)
	{
		// use water friction factor 0.1 - 0.9
		// 0.3 -> like water
		// 0.8 -> like honey
		const float_32 water_friction = 0.3f;

		// update velocity
		velocity += totale_force * delta_t * inv_mass;

		// friction inside water ...
		if(pos.y < water_height) {
			velocity -= velocity * inv_mass * delta_t * water_friction;
		}

		// (air) friction (to avoid endless sliding / motion and super fast things)
		const float_32 velocity_norm_squared = velocity.x * velocity.x + velocity.y * velocity.y;
		float_32 friction_factor = 1.0f - (velocity_norm_squared - 9000.0f)/ 32768.0f ;
		// limits
		// max 0.99999995f (bigger would be coded as 1.0f)
		if(friction_factor > 0.99999f) friction_factor = 0.99999f;
		if(friction_factor < 0.01f) friction_factor = 0.01f;

		velocity *= friction_factor;

		pos += velocity * delta_t;
		totale_force = {0,0}; //clear_force ();

#ifdef DEBUG
		// check for super fast things
		const float_32 max_velocity_squared = 200.0f * 200.0f;
		if (velocity_norm_squared > max_velocity_squared)
		{
			if(used_in_train)
				INFO_PRINT("physics: super fast things happen: train particle velocity reached %d\n", int(velocity.length()));
			else
				INFO_PRINT("physics: super fast things happen: particle velocity reached %d\n", int(velocity.length()));
		}
#endif
	}
}



