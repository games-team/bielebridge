//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//  Copyright (C) 2002 Ingo Ruhnke <grumbel@gmx.de>
//
//  This file is part of bielebridge  but was at least
//  partially developed for construo by Ingo Ruhnke.
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_SPRING_H
#define HEADER_SPRING_H

#include <vector>

class GUI_Editorsimulator;
class Particle;

class Spring
{
private:

	float_32 get_color(const float_32 color) const
	{
		//                        //  * 1.1f stretches contrast
		float_32 return_color = color * 1.1f / max_stretch;
		if(return_color < -1.0f) return_color = -1.0f;
		if(return_color > 1.0f) return_color = 1.0f;

		return return_color;
	}
public:
	// both colors need to be multiplicated by 1.1f / max_stretch and set to intverval [-1.0, 1.0];
	// color for live display in simulator
	float_32 cur_color = 0.0f;
	// store maximum stretch ratio color
	float_32 max_color = 0.0f;
	/** Pointers to the two particles to which the Spring is
	    connected */
	std::pair<Particle*, Particle*> particles;
	// index for simulation particles (sim_particles vector)
	// FIXME make this 3 members const again
	float_32 stiffness;
	float_32 damping;

	// max length in percent
	float_32 max_stretch;

	enum Spring_Type
	{
		BRIDGE = 0,
		DECK = 1,
		TRAIN = 2,
		COUPLING = 3
	};
	Spring_Type type;

	/** orignial length of the Spring, the actually length on the
	    screen is different, since the spring can be streched */
	float_32 length;
	float_32 inv_length;

	// mark spring as destroyed
	// values: 0 (normal)
	//         1 (destroyed)
	//         2 (destroyed and reported)
	enum Spring_Status
	{
		NORMAL = 0,
		DESTROYED = 1,
		DESTROYED_AND_REPORTED = 2
	};
	Spring_Status status = NORMAL;

	Spring (Particle* f, Particle* s, float_32 stiffness, float_32 damping, float_32 max_stretch, Spring::Spring_Type type);
	Spring (const Spring& s);
	Spring ();
	~Spring ();

	// if connected particles are similar (both directions)
	bool operator==(const Spring& s) const
	{
		return (particles.first == s.particles.first && particles.second == s.particles.second)
			|| (particles.first == s.particles.second && particles.second == s.particles.first);
	}

	void update();

	  /** Forces the recalculation of the springs length */
	void recalc_length ();

	float_32 get_cur_color() const
	{
		return get_color(cur_color);
	}
	float_32 get_max_color() const
	{
		return get_color(max_color);
	}
	float_32 get_max_color_raw() const
	{
		return max_color;
	}
	void set_max_color_raw(const float_32 color)
	{
		max_color = color;
	}
};

#endif
