//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <iostream>
#include "vector2d.hpp"
#include "level.hpp"
#include "spring.hpp"
#include "particle.hpp"
#include "error.hpp"
#include "gui_editor_undomanager.hpp"


GUI_Editor_Undomanager::GUI_Editor_Undomanager(){
}

GUI_Editor_Undomanager::~GUI_Editor_Undomanager(){
}

void GUI_Editor_Undomanager::save_current_state(const Level* level){
	Level* saved_level = new Level(*level);
	// copy everything...
	undo_level_list.push_back(saved_level);
}

Level* GUI_Editor_Undomanager::undo(Level* level){
	Level* undo_level = undo_level_list.back();
	undo_level_list.pop_back();
	//undo_level_list.push_back(level);
	return undo_level;
}

Level* GUI_Editor_Undomanager::redo(Level* level){
	Level* undo_level = undo_level_list.back();
	undo_level_list.pop_back();
	//undo_level_list.push_back(level);
	return undo_level;
}
