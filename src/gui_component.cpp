//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//  Copyright (C) 2002 Ingo Ruhnke <grumbel@gmx.de>
//
//  This file is part of bielebridge  but was at least
//  partially developed for construo by Ingo Ruhnke.
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <SDL2/SDL.h>
#include "gui_component.hpp"
#include "gui_manager.hpp"
#include "settings.hpp"

GUI_Component::GUI_Component (int x, int y, int width, int height, Alignment alignment)
	  : pos_x (x), pos_y (y), width (width), height (height), alignment(alignment)
{
	// calculate offsets for later repositioning after resize
	switch(alignment)
	{
		case RES_INDEPENDENT:
			break;
		case LEFT_UPPER:
			alignment_x_offset = pos_x;
			alignment_y_offset = pos_y;
			break;
		case CENTER_UPPER:
			alignment_x_offset = Settings::singleton()->window_width/2 - pos_x;
			alignment_y_offset = pos_y;
			break;
		case RIGHT_UPPER:
			alignment_x_offset = Settings::singleton()->window_width - pos_x;
			alignment_y_offset = pos_y;
			break;
		case LEFT_CENTER:
			alignment_x_offset = pos_x;
			alignment_y_offset = Settings::singleton()->window_height/2 - pos_y;
			break;
		case CENTER_CENTER:
			alignment_x_offset = Settings::singleton()->window_width/2 - pos_x;
			alignment_y_offset = Settings::singleton()->window_height/2 - pos_y;
			break;
		case RIGHT_CENTER:
			alignment_x_offset = Settings::singleton()->window_width - pos_x;
			alignment_y_offset = Settings::singleton()->window_height/2 - pos_y;
			break;
		case LEFT_LOWER:
			alignment_x_offset = pos_x;
			alignment_y_offset = Settings::singleton()->window_height - pos_y;
			break;
		case CENTER_LOWER:
			alignment_x_offset = Settings::singleton()->window_width/2 - pos_x;
			alignment_y_offset = Settings::singleton()->window_height - pos_y;
			break;
		case RIGHT_LOWER:
			alignment_x_offset = Settings::singleton()->window_width - pos_x;
			alignment_y_offset = Settings::singleton()->window_height - pos_y;
			break;
	}

	recalc_offsets(alignment, pos_x, pos_y, alignment_x_offset, alignment_y_offset);
}

void GUI_Component::recalc_offsets(const Alignment a, const int pos_x, const int pos_y, int& ret_x_offset, int& ret_y_offset){
}

bool GUI_Component::is_at (int x, int y)
{
	if(x >= this->get_x_pos() && y >= this->get_y_pos() &&
	   x <  this->get_x_pos() + this->get_width() &&
	   y <  this->get_y_pos() + this->get_height())
		return true;
	else
		return false;
}

// recalc new pos_x and pos_y from alignment offsets
void GUI_Component::on_window_resize ()
{
	switch(alignment)
	{
		case RES_INDEPENDENT:
			break;
		case LEFT_UPPER:
			break;
		case CENTER_UPPER:
			pos_x = Settings::singleton()->window_width/2 - alignment_x_offset;
			break;
		case RIGHT_UPPER:
			pos_x = Settings::singleton()->window_width - alignment_x_offset;
			break;
		case LEFT_CENTER:
			pos_y = Settings::singleton()->window_height/2 - alignment_y_offset;
			break;
		case CENTER_CENTER:
			pos_x = Settings::singleton()->window_width/2 - alignment_x_offset;
			pos_y= Settings::singleton()->window_height/2 - alignment_y_offset;
			break;
		case RIGHT_CENTER:
			pos_x = Settings::singleton()->window_width - alignment_x_offset;
			pos_y = Settings::singleton()->window_height/2 - alignment_y_offset;
			break;
		case LEFT_LOWER:
			pos_y = Settings::singleton()->window_height - alignment_y_offset;
			break;
		case CENTER_LOWER:
			pos_x = Settings::singleton()->window_width/2 - alignment_x_offset;
			pos_y = Settings::singleton()->window_height - alignment_y_offset;
			break;
		case RIGHT_LOWER:
			pos_x= Settings::singleton()->window_width - alignment_x_offset;
			pos_y = Settings::singleton()->window_height - alignment_y_offset;
			break;
	}
}

int GUI_Component::get_x_pos()
{
	if(alignment == RES_INDEPENDENT)
		return int(float_32(Settings::singleton()->window_width * pos_x) / 1000.0f);
	else
		return pos_x;
}

int GUI_Component::get_y_pos()
{
	if(alignment == RES_INDEPENDENT)
		return int(float_32(Settings::singleton()->window_height * pos_y) / 1000.0f);
	else
		return pos_y;
}

int GUI_Component::get_x_plus_width_pos()
{
	if(alignment == RES_INDEPENDENT)
	{
		float res_ind_x_pos = float_32(Settings::singleton()->window_width * pos_x) / 1000.0f;
		float res_ind_width = float_32(Settings::singleton()->window_width * width) / 1000.0f;
		return int(res_ind_x_pos + res_ind_width );
	}
	else
		return pos_x + width;
}

int GUI_Component::get_y_plus_height_pos()
{
	if(alignment == RES_INDEPENDENT)
	{
		float res_ind_y_pos = float_32(Settings::singleton()->window_height * pos_y) / 1000.0f;
		float res_ind_height = float_32(Settings::singleton()->window_height * height) / 1000.0f;
		return int( res_ind_y_pos + res_ind_height );
	}
	else
		return pos_y + height;
}

int GUI_Component::get_width()
{
	if(alignment == RES_INDEPENDENT)
		return int(float_32(Settings::singleton()->window_width * width) / 1000.0f);
	else
		return width;
}

int GUI_Component::get_height()
{
	if(alignment == RES_INDEPENDENT)
		return int(float_32(Settings::singleton()->window_height * height) / 1000.0f);
	else
		return height;
}

/* EOF */
