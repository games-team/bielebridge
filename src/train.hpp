//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_TRAIN_H
#define HEADER_TRAIN_H

// specification for all train springs
#define TRAIN_SPRING_STIFFNESS 265000.0f
#define TRAIN_SPRING_DAMPING 50.1f
#define TRAIN_SPRING_MAX_STRETCH 0.15f

#define COUPLING_SPRING_STIFFNESS 53000.0f
#define COUPLING_SPRING_DAMPING 1.0f
#define COUPLING_SPRING_MAX_STRETCH 0.1f

class Particle;
class Spring;
class Level;

// A Locomotive is build with Particles and Springs. Numeration ist showen in source code.

class Locomotive
{
	friend class Train;
	friend class Physics;
	friend class GUI_Simulator;
public:
	static const int points_num = 5;
	static const int springs_num = 10;
private:
	Particle* points[points_num];
	Spring* springs[springs_num];

	Particle* front_coupling;
	Particle* back_coupling;
	Particle* back_coupling_top;

	Locomotive(float_32 weight, float_32 pos, Level* level);
};


/*
*   3---0   3---4
*   |\ /|  /|\ / \
*   | X | / | /\  0
*   |/ \|/  |/  \/
*   2---1---2---1
*/

class Carriage
{
	friend class Train;
	friend class Physics;
	friend class GUI_Simulator;
public:
	static const int points_num = 4;
	static const int springs_num = 6;
private:
	Particle* points[points_num];
	Spring* springs[springs_num];

	Particle* front_coupling;
	Particle* back_coupling;
	Particle* back_coupling_top;

	Carriage(float_32 weight, float_32 xpos, float_32 ypos, Level* level);
};

class Train
{

public:
	const float_32 tractive_power = 10000.0f;
	const float_32 velocity_target = 30.0f;
	int num_loc;
	int num_couplings;
	Locomotive** locomotives;

	int num_car;
	Carriage** carriages;

	Spring** couplings;

	float_32 start_pos;

public:
	// need the level parameter for registering the particles and springs
	Train(float_32 xpos, float_32 ypos, int num_loc, float_32 loc_weight, int num_car, float_32 car_weight, Level* level);
	~Train();

	void check_train_and_couplings();
	int get_speed();

	void add_debug_car(float_32 x, float_32 y, Level* level);
	Vector2d get_pos_for_action_cam() const {
		return locomotives[0]->points[0]->pos;
	}
	void get_train_points_by_p(const Particle* p, Vector2d vertices[], int& vertices_num) const;
	void get_springs_to_p(const Particle* p, Spring*& s1, Spring*& s2) const;
};

#endif

