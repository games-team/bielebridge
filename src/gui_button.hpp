//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_GUI_BUTTON_H
#define HEADER_GUI_BUTTON_H

#include <string>
#include <SDL2/SDL.h>
#include <GL/gl.h>

#include "gui_component.hpp"

using namespace std;

class GUI_Component;

/**
 * Used as button and (not click able) text
 */
class GUI_Button : public GUI_Component
{
private:
	SDL_Color text_color;
	SDL_Color highlight_color;
	int font_size;
	std::string text;

	// Text as Image:
	// pointer to SDL texture (only used with SDL internal renderer)
	SDL_Texture *sdl_texture = nullptr;
	// pointer to SDL OpenGL texture
	GLuint opengl_texture = 0;
	int image_width, image_height;

	// options
	bool background = true;
	bool highlight = false; // more "violent" background color

	bool need_rerender = true;

	int last_window_w = 0;
	int last_window_h = 0;


public:
	GUI_Button (const std::string &text_, int pos_x, int pos_y, int width, int height, Alignment a = LEFT_UPPER);
	~GUI_Button ();

	void         set_sdl_texture(SDL_Texture* t){ sdl_texture = t; }
	SDL_Texture* get_sdl_texture(){return sdl_texture; }

	void   set_opengl_texture(GLuint t){ opengl_texture = t; }
	GLuint get_opengl_texture(){return opengl_texture; }

	void set_need_rerender(bool n){need_rerender = n;}
	bool get_need_rerender();

	void set_image_width(int w){image_width = w;}
	void set_image_height(int h){image_height = h;}
	int  get_image_width(){return image_width;}
	int  get_image_height(){return image_height;}

	void set_background(bool b){ background = b; }
	bool get_background(){return background; }

	void set_highlight(bool h){ highlight = h; }
	bool get_highlight(){return highlight; }

	void      set_highlight_color(const SDL_Color c){highlight_color = c;}
	SDL_Color get_highlight_color(){return highlight_color;}

	void      set_text_color(const SDL_Color c){text_color = c;}
	SDL_Color get_text_color(){return text_color;}

	void set_last_window_w(const int w){ last_window_w = w;}
	void set_last_window_h(const int h){ last_window_h = h;}
	int get_last_window_w(){return last_window_w;}
	int get_last_window_h(){return last_window_h;}

	void set_new_text(const std::string& new_text);
	std::string get_text(){return text;}

	void set_ris(const int x_permille, const int y_permille, const int width_permille, const int height_permille);
};

#endif

