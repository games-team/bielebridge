//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//  Copyright (C) 2002 Ingo Ruhnke <grumbel@gmx.de>
//
//  This file is part of bielebridge  but was at least
//  partially developed for construo by Ingo Ruhnke.
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_VECTOR2D_H
#define HEADER_VECTOR2D_H

#include <iostream>
#include <cmath>

#include "settings.hpp"

class Vector2d
{
public:
  float_32 x;
  float_32 y;

  Vector2d ()
    : x(0), y(0)
  {}

  Vector2d (float_32 x, float_32 y)
    : x (x), y (y)
  {}

  Vector2d (const Vector2d& other)
    :x(other.x), y(other.y)
  {}

  void operator+=(const Vector2d& vec) {
    x += vec.x;
    y += vec.y;
  }

  void operator-=(const Vector2d& vec) {
    x -= vec.x;
    y -= vec.y;
  }

  void operator*=(const float_32 f) {
    x *= f;
    y *= f;
  }

  void operator/=(const float_32 f) {
    x /= f;
    y /= f;
  }

  // be warned, floating point comparison!
  bool operator==(const Vector2d& vec) const {
    return x == vec.x && y == vec.y;
  }

  Vector2d operator+(const Vector2d& vec) const  {
    return Vector2d(x + vec.x, y + vec.y);
  }

  // dt. Skalarprodukt
  float_32 dot(const Vector2d& vec) const {
    return (x * vec.x) + (y * vec.y);
  }


  Vector2d operator-() const  {
    return Vector2d(-x, -y);
  }

  Vector2d operator-(const Vector2d& vec) const  {
    return Vector2d(x - vec.x, y - vec.y);
  }

  Vector2d operator*(const float_32 f) const  {
    return Vector2d(x * f, y * f);
  }

  Vector2d operator/(const float_32 f) const  {
    return Vector2d(x / f, y / f);
  }


  /** @return the length of the vector, also known as norm
   * */
  float_32 length() const {
    return std::sqrt (x*x + y*y);
  }


  /** @return the length of the vector squared */
  float_32 length_square() const {
    return x*x + y*y;
  }

  /** set to length 1.0 */
  void normalize() {
    float_32 f = std::sqrt (x*x + y*y);
    if (f != 0)
      {
        x /= f;
        y /= f;
      }
  }

  static
  float_32 dot(const Vector2d& a, const Vector2d& b) {
    return (a.x * b.x) + (a.y * b.y);
  }

  static
  float_32 distance(const Vector2d& a, const Vector2d& b) {
    return (a - b).length();
  }

  static
  float_32 distance_square(const Vector2d& a, const Vector2d& b) {
    return (a - b).length_square();
  }

  static
  Vector2d average(const Vector2d& a, const Vector2d& b) {
    return (a + b)* 0.5f;
  }
};

inline
std::ostream& operator<<(std::ostream& os, const Vector2d& v)
{
  return os << "[" << v.x << ", " << v.y << "]";
}


#endif

/* EOF */
