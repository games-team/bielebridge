//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_GAME_H
#define HEADER_GAME_H

class GUI_Manager;

class Game {
private:
	// Window and renderer
	SDL_Renderer* renderer;                 // holds rendering surface properties
	SDL_Window* window;
	GUI_Manager* guimanager;

	// FPS display in window title
	unsigned int frame_counter;
	unsigned int timems_last_reset;
	unsigned int current_fps;

	char window_title[100];
	bool keyboard_input_uppercase = false;

public:
	Game(SDL_Renderer* renderer, SDL_Window* window);
	void loop();
	void input();
	void update();
	void render();

	int* mouse_x_ptr,* mouse_y_ptr;
	bool exit;
};

#endif

