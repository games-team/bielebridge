//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_GUI_EDITOR_H
#define HEADER_GUI_EDITOR_H


#include "gui_editor_undomanager.hpp"

class GUI_Button;
class GUI_Component;
class GUI_Manager;
class GUI_Editorsimulator;
class GUI_Editor_Undomanager;

class GUI_Editor : public GUI_Editorsimulator
{
private:
	GUI_Editor_Undomanager undomanager;

	GUI_Button* run_button;
	GUI_Button* save_and_back_button;
	GUI_Button* back_button;
	GUI_Button* ground_button;
	GUI_Button* anchor_button;
	GUI_Button* water_button;
	GUI_Button* error_button;
	GUI_Button* clear_button;
	GUI_Button* undo_button;
	GUI_Button* redo_button;
	GUI_Button* deck_button;
	GUI_Button* level_name_button;
	GUI_Button* budget_button;
	GUI_Button* budget_used_button;
	GUI_Button* budget_left_button;
	GUI_Button* max_stress_button;
	GUI_Button* level_width_plus_button;
	GUI_Button* level_width_minus_button;
	GUI_Button* level_shift_right_button;
	GUI_Button* level_shift_left_button;
	GUI_Button* first_level_tooltip_text;
	GUI_Button* copy_paste_level_tooltip_text;
	GUI_Button* deck_level_tooltip_text;
	GUI_Button* help_text;
	GUI_Button* help_button;

	GUI_Button* copy_button;
	GUI_Button* delete_button;
	GUI_Button* flip_x_button;
	bool draw_copy_rect_with_mouse = false;
	float_32 mark_rect_x, mark_rect_y, mark_rect_w, mark_rect_h;

	typedef std::vector<GUI_Component*> Component_List;
	Component_List buttons;

	enum editor_mode { REGULAR, REGULAR_DECK, REGULAR_COPY_MARK, REGULAR_COPY_MOVE, GROUND, ANCHOR, WATER };
	editor_mode current_editor_mode;

	bool display_max_stress = false;
	const float_32 particle_mass = 0.1f;
	int budget_left;
	char budget_str[20];

	// REGULAR mode
	/** while creating a spring */
	Spring* new_spring = nullptr;
	bool new_spring_visible; // might be invisible if too long or particle inside ground

	/** particle of a new spring is already set */
	Particle* new_particle_mouse = nullptr; // mouse moving
	Particle* new_particle_base = nullptr; // base

	/** to draw elements highlighted and to unhighlight them */
	Spring* highlighted_spring;
	Particle* highlighted_particle;

	// GROUND mode
	int cur_ground_index;


	////////////////////////////////////////////////////////////////////////////////
	// Multi Spring Copy Mode
	int copy_mode_springs_num = 0;
	void passive_mouse_motion();
	void copy_mode_mouse_motion();
	void copy_mode_mouse_motion_move(const bool place_it);

	/** returns the nearest Spring/Particle to the cursor */
	// FIXME: delete these functions (use functions of level::)
	Spring* get_spring_by_xy(float_32 x, float_32 y);
	Particle* get_particle_by_xy (float_32 x, float_32 y, const float_32 catch_distance = 5.0f);

	void event_mouse_left_down(float_32 x, float_32 y);
	void event_mouse_right_down(float_32 x, float_32 y);

	void clear_mouse_state();
	void clear_bridge();

	void update_budget();

	void draw_all_springs() const;
	void draw_all_particles() const;
	void draw_all_buttons() const;

#ifdef DEBUG
	static const int EDITOR_FOOTER_SIZE = 120;
	char editor_footer[EDITOR_FOOTER_SIZE];
#endif
	void draw_help() const;

public:
	GUI_Editor(const int level_id, const std::string& level_name, const std::string& level_absolute_path);
	virtual ~GUI_Editor();
	void draw() const;
	void update();

	/** process button clicks and translates screen x,y to world x,y */
	void event_mouse_click(GUI_Manager* guimanager, int button, int button_state, int x, int y);

	void event_keyboard(GUI_Manager* guimanager, const char* utf8_key);
	void event_motion(int x, int y);
	void event_window_resize();

	void user_pressed_help();
};

#endif

