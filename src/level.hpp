//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_LEVEL_H
#define HEADER_LEVEL_H

#include <vector>
#include "spring.hpp"
#include "particle.hpp"

class Train;


//! (game)level contains all information of a game level

class Level
{
	friend class GUI_Editor;
	friend class GUI_Simulator;
	friend class GUI_Editorsimulator;
	friend class Benchmark;
	friend class Train;

private:
	int id; // starts with 1 for predefined levels (user levels have id == 0)
	int particle_id_count = 0;
	std::string name = ""; // for display
	std::string full_path = ""; // absolute path + level filename + filename extension


// FIXME: should be private
public:
	// temporary public
	/** Level width must be divisible by 5 */
	int width = 0;
	const float_32 ground_left_outside = 0.0f;
	const float_32 ground_right_outside = 0.0f;

	float_32* ground = nullptr;
	int ground_len = 0;
	float_32 water_height = -20.0f;
	int budget;
	int budget_left; // needed bei gui_editor

	// springs and particles
	std::vector<Spring*> springs;
	typedef std::vector<Spring*>::iterator Spring_iter;

	std::vector<Particle*> particles;
	typedef std::vector<Particle*>::iterator Particle_iter;

	// springs and particles for simulation
	// if simulation starts they are copied from springs and particles
	// to sim_springs and sim_particles
	// then simulation ends max_colors there is copied back
	std::vector<Particle> sim_particles;
	typedef std::vector<Particle>::iterator Particle_iters;
	std::vector<Spring> sim_springs;
	typedef std::vector<Spring>::iterator Spring_iters;
	// indices of deck springs of vector sim_springs
	std::vector<uint_fast32_t> sim_deck_springs_i;

	////////////////////////////////////////////////////////////////////////////////
	// Multi Spring Copy Mode
	// pointes to copied springs and particles (for mark, copy & paste in GUI_Editor)
	std::vector<Particle*> copy_particles;
	std::vector<Spring*> copy_springs; // particles.first/second point to copied particles
	void free_copied_particles_and_springs();
	void add_copied_particles_and_springs();
	void flip_x_copied_particles_and_springs();
	void delete_springs_from_area(const float_32 x, const float_32 y, const float_32 w, const float_32 h);
	void copy_particles_and_springs_inside_rect(const float_32 x, const float_32 y, const float_32 w, const float_32 h);
	// moves particles to given position (mouse pointer)
	void move_copied_particles_and_springs_to(const float_32 x, const float_32 y);
	unsigned int get_new_springs_num(const float_32 x, const float_32 y);

	int load_from_file();
	int save_to_file();

	bool level_passed;
	bool editable;
	bool custom_deck; // user is able to decide where to place the deck

public:
	bool load_failed = false;
	Level(); // crate new default level
	Level(int id, const std::string full_path, const std::string name); // load predefined level with id (id == 0 means user level)
	~Level();
	Level(const Level& level_to_copy); // copy constructor (deep copy)

	int get_id(){return id;};
	std::string get_name(){return name;};
	void set_full_path(const std::string path){full_path = path;}

	void copy_for_simulation();
	void free_after_simulation();

	// ADD and DELETE particles and springs
	Particle* add_particle(const Vector2d& pos, const float_32 mass, const bool fixed, const bool used_in_train=0);
	Particle* add_sim_particle(const Vector2d& arg_pos, float_32 m, bool fixed, bool used_in_train=0);
	void delete_particle (Particle* p, bool force=false);
	Spring* add_spring(Particle* p1, Particle* p2, float_32 stiffness, float_32 damping, float_32 max_stretch, Spring::Spring_Type type);
	Spring* add_sim_spring(Particle* p1, Particle* p2, float_32 stiffness, float_32 damping, float_32 max_stretch, Spring::Spring_Type type);

	void delete_spring(Spring* s);
	// helper function for clear Bridge
	void clear_bridge();

	// CHECKS, INFO
	// retruns pointer to nearest particle to "pos" out of "p_vec"
	Particle* get_nearest_particle(const Vector2d& pos, std::vector<Particle*>& p_vec);

	float_32 get_ground_height_at(const float_32 x_pos) const;
	bool anchor_near(const Vector2d& pos, const float_32 distance);
	bool on_spring_line(const Vector2d& pos);
	bool p_inside_ground(const Vector2d& pos) const;
	int get_double_cross_train_line(const int ground_index, const Vector2d* train_points, const int train_points_num) const;
	bool s_double_cross_with_same_ground(const Spring* s1, const Spring* s2, Vector2d& ground_before, Vector2d& ground_center ,Vector2d& ground_after) const;

	int fit_ground_index_to_boundaries(const int index) const;
	void copy_max_colors_after_simulation();
	void adjust_level_width(const int increase);

	void level_shift_left(const int amount);
};

#endif
