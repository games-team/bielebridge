//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//  Copyright (C) 2002 Ingo Ruhnke <grumbel@gmx.de>
//
//  This file is part of bielebridge  but was at least
//  partially developed for construo by Ingo Ruhnke.
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <cmath>
#include <vector>
#include "vector2d.hpp"
#include "particle.hpp"
#include "spring.hpp"
#include "train.hpp"
#include "level.hpp"
#include "math_utils.hpp"
#include "error.hpp"

Spring::Spring (Particle* f, Particle* s, float_32 stiffness, float_32 damping, float_32 max_stretch , Spring_Type type)
	:stiffness(stiffness), damping(damping), max_stretch(max_stretch), type(type), status(Spring::NORMAL)
{
	particles.first   = f;
	particles.second  = s;
	length           = (float_32) fabs((f->pos - s->pos).length());

	particles.first->spring_links  += 1;
	particles.second->spring_links += 1;

	if(f==s) ERROR_PRINT("Spring (IDs: %d %d) with same start and end Particle\n", f->id, s->id);
}

Spring::Spring (const Spring& s)
	: particles(s.particles),
	stiffness(s.stiffness),
	damping(s.damping),
	max_stretch(s.max_stretch),
	type(s.type),
	status(s.status)
{}

Spring::Spring ()
{
	particles.first = nullptr;
	particles.second = nullptr;
	stiffness = 1.0f;
	damping = 1.0f;
	max_stretch = 2.0f;
	type = Spring::BRIDGE;
	status = Spring::NORMAL;
	length = 1.0f;
}

Spring::~Spring ()
{
	particles.first->spring_links  -= 1;
	particles.second->spring_links -= 1;
}


/** calculates forces to particles of this spring or
 * destroies spring if forces are to high
 *
 * Note: This function is responsible for about 50% of physics runtime!
 */
void Spring::update()
{
	// not destroyed
	if(status == Spring::NORMAL)
	{
		Particle& first = *particles.first;
		Particle& second = *particles.second;
		Vector2d spring_vec = first.pos - second.pos;
		// Calculate the stretchness of the spring, 0.0 if unstretch, else <> 0
		const float_32 cur_length = spring_vec.length();
		float_32 stretch = (cur_length - length);
		//const float_32 inv_length = 1.0f / length;
		if (fabs(stretch * inv_length) > max_stretch)
		{ // If the spring is streched above limits, let it get destroyed
			status = Spring::DESTROYED;
			if(stretch > 0 )
				max_color = 1.0f;
			else
				max_color = -1.0f;
		}
		else
		{
			// divided by length, this gives long springs equal strength as short ones
			stretch *= stiffness * inv_length;
			//const float_32 dterm = (spring_vec.dot(particles.first->velocity - particles.second->velocity) * damping)/spring_vec.length();
			const float_32 dterm = (spring_vec.dot(first.velocity - second.velocity) * damping)/cur_length;

			spring_vec.normalize();
			const Vector2d force = spring_vec * (stretch + dterm);

			first.totale_force += -force;
			second.totale_force += force;
		}

		// calcualte color
		// ratio                                       //  * 1.1f stretches contrast
		cur_color = (cur_length * inv_length - 1.0f);// / max_stretch) * 1.1f;
//
//		// streched contrast may violate bondaries, so enforce them
//		if(cur_color < -1.0f) cur_color = -1.0f;
//		if(cur_color > 1.0f) cur_color = 1.0f;

		// update max_color
		if(fabs(cur_color) > fabs(max_color))
			max_color = cur_color;
	}
}

void Spring::recalc_length (){
	length = (float_32) fabs((particles.first->pos - particles.second->pos).length());
}

/*
float_32
Spring::get_stretch_color() const
{
	// get current spring length
	const float_32 cur_length = Vector2d::distance(particles.first->pos, particles.second->pos);
	// ratio                                      * 1.1f stretches contrast
	float_32 color = (cur_length / length - 1.0f) * 1.1f / max_stretch;

	// streched contrast may violate bondaries, so enforce them
	if(color < -1.0f) color = -1.0f;
	if(color > 1.0f) color = 1.0f;
	return color;
}
*/
