//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.
#include <SDL2/SDL2_gfxPrimitives.h>
#include <iostream>
#include <string>
#include <vector>
#include "color4f.hpp"
#include "vector2d.hpp"
#include "particle.hpp"
#include "spring.hpp"
#include "gui_menu_layouts.hpp"
#include "gui_manager.hpp"
#include "sdl_draw.hpp"
#include "gui_editorsimulator.hpp"
#include "level.hpp"
#include "settings.hpp"
#include "error.hpp"
#include "math_utils.hpp"


void GUI_Editorsimulator::event_keyboard(const char* utf8_key)
{
	std::string utf8_string = utf8_key;
	// move
	if( utf8_string.compare("w") == 0 || utf8_string.compare("Up")==0) sdl->move_up();
	if( utf8_string.compare("s") == 0 || utf8_string.compare("Down")==0) sdl->move_down();
	if( utf8_string.compare("a") == 0 || utf8_string.compare("Left")==0) sdl->move_left();
	if( utf8_string.compare("d") == 0 || utf8_string.compare("Right")==0) sdl->move_right();

	// zoom
	if( utf8_string.compare("q") == 0 ) sdl->zoom_out();
	if( utf8_string.compare("e") == 0 ) sdl->zoom_in();
}

void GUI_Editorsimulator::event_mouse_wheel(int32_t y)
{
	if(y < 0) sdl->zoom_out();
	else sdl->zoom_in();
}

