//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_GUI_SIMULATOR_H
#define HEADER_GUI_SIMULATOR_H

#include "physics.hpp"

class GUI_Editorsimulator;
class GUI_Button;
class GUI_Component;
class GUI_Manager;

class GUI_Simulator : public GUI_Editorsimulator
{
private:
	GUI_Button* back_button;
	GUI_Button* faster_button;
	GUI_Button* very_fast_button;
	GUI_Button* slower_button;
	GUI_Button* pause_button;
	GUI_Button* pause_broken_button;
	GUI_Button* level_name_button;
	GUI_Button* level_passed_button;
	GUI_Button* next_level_button;
	GUI_Button* action_cam_button;

	typedef std::vector<Particle>::iterator Particle_iters;
	typedef std::vector<Spring>::iterator Spring_iters;

	typedef std::vector<GUI_Component*> Component_List;
	Component_List buttons;

	int animation_speed;
	int prev_animation_speed = 0;
	time_t current_sec;

	// frames / sec
	int fps;
	int count_frames;

	// iterations / sec
	int ips;
	int count_iter;
	unsigned long count_all_iter;

	bool pause_on_broken_spring;
	bool action_cam;

	Physics* physics;
#ifdef DEBUG_SIMULATOR
	// for debug
	static const int SIM_FOOTER_SIZE = 120;
	char sim_footer[SIM_FOOTER_SIZE];
#endif

public:
	GUI_Simulator(Level* l);
	virtual ~GUI_Simulator();
	void physics_draw();
	void draw();
	void update();
	void event_mouse_click(GUI_Manager* guimanager, int button, int button_state, int x, int y);
	void event_keyboard(const char* utf8_key);
	void event_window_resize();
	void draw_broken_springs();
	void debug_draw();

	// if pause_on_broken_spring is set display broken springs
	std::vector<Spring*> broken_springs;
};

#endif
