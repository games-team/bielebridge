//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include "color4f.hpp"
#include "gui_component.hpp"
#include "gui_button.hpp"
#include "error.hpp"
#include "settings.hpp"

GUI_Button::GUI_Button(const string &text_, int pos_x, int pos_y, int width, int height, Alignment a)
		: GUI_Component(pos_x, pos_y, width, height, a)
{
	text = text_;
	text_color = { 255, 255, 255, 255 }; // white
	highlight_color = Settings::singleton()->sdlc_button_highlight;
}

GUI_Button::~GUI_Button()
{
	if(opengl_texture != 0)
	{
		glDeleteTextures(1, &opengl_texture);
	}
	if(sdl_texture != nullptr)
	{
		SDL_DestroyTexture(sdl_texture);
	}
}

void GUI_Button::set_new_text(const std::string& new_text)
{
	text = new_text;
	need_rerender = true;
}

bool GUI_Button::get_need_rerender()
{
	// check if window size has changed and button is resolution independet
	// elsewise this causes missized textures
	if(alignment == RES_INDEPENDENT)
	{
		int cur_h = Settings::singleton()->window_height;
		int cur_w = Settings::singleton()->window_width;
		if( cur_h != last_window_h || cur_w != last_window_w)
			return true;
	}
	return need_rerender;
}

