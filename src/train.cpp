//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <vector>
#include <cmath>
#include "vector2d.hpp"
#include "particle.hpp"
#include "spring.hpp"
#include "train.hpp"
#include "level.hpp"
#include "math_utils.hpp"
#include "error.hpp"


Locomotive::Locomotive(float_32 weight, float_32 pos, Level* level)
{
	points[0]=level->add_sim_particle( Vector2d(pos, 15), weight/5, false, 1 );
	points[1]=level->add_sim_particle( Vector2d(pos-10, 5), weight/5, false, 1 );
	points[2]=level->add_sim_particle( Vector2d(pos-60, 5), weight/5, false, 1 );
	points[3]=level->add_sim_particle( Vector2d(pos-60, 35), weight/5, false, 1 );
	points[4]=level->add_sim_particle( Vector2d(pos-20, 35), weight/5, false, 1 );

	// shape
	springs[0]=level->add_sim_spring(points[0], points[1], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[1]=level->add_sim_spring(points[1], points[2], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[2]=level->add_sim_spring(points[2], points[3], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[3]=level->add_sim_spring(points[3], points[4], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[4]=level->add_sim_spring(points[4], points[0], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	// cross
	springs[5]=level->add_sim_spring(points[0], points[2], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[6]=level->add_sim_spring(points[1], points[3], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[7]=level->add_sim_spring(points[2], points[4], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[8]=level->add_sim_spring(points[1], points[4], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[9]=level->add_sim_spring(points[0], points[3], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;

	front_coupling = points[1];
	back_coupling = points[2];
	back_coupling_top = points[3];
}


///// Carriage /////////////////////////////////////////////////////////////
Carriage::Carriage(float_32 weight, float_32 xpos, float_32 ypos, Level* level)
{
	points[0]=level->add_sim_particle( Vector2d(xpos, ypos+ 35), weight/4, false, 1 );
	points[1]=level->add_sim_particle( Vector2d(xpos, ypos+ 5), weight/4, false, 1 );
	points[2]=level->add_sim_particle( Vector2d(xpos-60, ypos+ 5), weight/4, false, 1 );
	points[3]=level->add_sim_particle( Vector2d(xpos-60, ypos+ 35), weight/4, false, 1 );
	// shape
	springs[0]=level->add_sim_spring(points[0], points[1], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[1]=level->add_sim_spring(points[1], points[2], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[2]=level->add_sim_spring(points[2], points[3], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[3]=level->add_sim_spring(points[3], points[0], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	// cross
	springs[4]=level->add_sim_spring(points[0], points[2], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;
	springs[5]=level->add_sim_spring(points[1], points[3], TRAIN_SPRING_STIFFNESS, TRAIN_SPRING_DAMPING, TRAIN_SPRING_MAX_STRETCH, Spring::TRAIN ) ;

	front_coupling = points[1];
	back_coupling = points[2];
	back_coupling_top = points[3];
}

///// Train ////////////////////////////////////////////////////////////////
Train::Train(float_32 xpos, float_32 ypos, int num_loc, float_32 loc_weight, int num_car, float_32 car_weight, Level *level)
	:num_loc(num_loc), num_car(num_car)
{
	num_couplings = (num_loc + num_car -1)*2;

	locomotives = new Locomotive* [num_loc];
	carriages = new Carriage* [num_car];
	couplings = new Spring* [num_couplings];

	for(int i=0; i < num_loc; ++i)
	{
		locomotives[i] = new Locomotive(loc_weight, float_32(-i)*70.0f + xpos, level);
		// set velocity to velocity_target
		for( int p = 0 ; p < Locomotive::points_num; ++p)
		{
			locomotives[i]->points[p]->velocity = Vector2d(velocity_target,0);
		}
	}

	for(int i=0; i< num_car; ++i)
	{
		carriages[i] = new Carriage(car_weight, float_32(-(num_loc + i))*70.0f +xpos, 0, level);
		// set velocity to velocity_target
		for( int p = 0 ; p < Carriage::points_num; ++p)
		{
			carriages[i]->points[p]->velocity = Vector2d(velocity_target,0);
		}
	}

	// create couplings
	for(int i=0; i< num_loc + num_car -1; i++)
	{
		if(i < num_loc-1)
		{
			couplings[2*i]   = level->add_sim_spring( locomotives[i]->back_coupling, locomotives[i+1]->front_coupling,
									COUPLING_SPRING_STIFFNESS, COUPLING_SPRING_DAMPING, COUPLING_SPRING_MAX_STRETCH, Spring::COUPLING );
			couplings[2*i+1] = level->add_sim_spring( locomotives[i]->back_coupling_top, locomotives[i+1]->front_coupling,
									COUPLING_SPRING_STIFFNESS, COUPLING_SPRING_DAMPING, COUPLING_SPRING_MAX_STRETCH, Spring::COUPLING );
		}

		if( i == num_loc -1)
		{
			couplings[2*i]   = level->add_sim_spring( locomotives[i]->back_coupling, carriages[0]->front_coupling,
									COUPLING_SPRING_STIFFNESS, COUPLING_SPRING_DAMPING, COUPLING_SPRING_MAX_STRETCH, Spring::COUPLING );
			couplings[2*i+1] = level->add_sim_spring( locomotives[i]->back_coupling_top, carriages[0]->front_coupling,
									COUPLING_SPRING_STIFFNESS, COUPLING_SPRING_DAMPING, COUPLING_SPRING_MAX_STRETCH, Spring::COUPLING );
		}

		if( i > num_loc -1)
		{
			couplings[2*i] =   level->add_sim_spring( carriages[i-num_loc]->back_coupling, carriages[i-num_loc+1]->front_coupling,
									COUPLING_SPRING_STIFFNESS, COUPLING_SPRING_DAMPING, COUPLING_SPRING_MAX_STRETCH, Spring::COUPLING );
			couplings[2*i+1] = level->add_sim_spring( carriages[i-num_loc]->back_coupling_top, carriages[i-num_loc+1]->front_coupling,
									COUPLING_SPRING_STIFFNESS, COUPLING_SPRING_DAMPING, COUPLING_SPRING_MAX_STRETCH, Spring::COUPLING );
		}
	}
}

Train::~Train()
{
	for(int i=0; i < num_loc; ++i)
		delete locomotives[i];

	for(int i=0; i< num_car; ++i)
		delete carriages[i];

	delete[] locomotives;
	delete[] carriages;
	delete[] couplings;
}

void Train::check_train_and_couplings()
{
	for(int s=0; s< num_couplings; s+=2)
	{
		// if one coupling is destroyed, kill both
		if(couplings[s]->status == Spring::DESTROYED || couplings[s+1]->status == Spring::DESTROYED)
		{
			couplings[s]->status = Spring::DESTROYED;
			couplings[s+1]->status = Spring::DESTROYED;
		}
		if(couplings[s]->status == Spring::DESTROYED_AND_REPORTED || couplings[s+1]->status == Spring::DESTROYED_AND_REPORTED)
		{
			couplings[s]->status = Spring::DESTROYED_AND_REPORTED;
			couplings[s+1]->status = Spring::DESTROYED_AND_REPORTED;
		}
	}

	for(int i=0; i < num_loc; ++i)
	{
		bool loc_valid = true;
		for(int j=0; j < 5; ++j)
		{
			if( locomotives[i]->springs[j]->status != Spring::NORMAL)
			{
				loc_valid = false;
			}
		}

		if(! loc_valid){
			// delete this invalid loc
		}
	}

	for(int i=0; i< num_car; ++i)
	{
		bool car_valid = true;
		for(int j=0; j < 4; ++j)
		{
			if( carriages[i]->springs[j]->status != Spring::NORMAL)
			{
				car_valid = false;
			}
		}

		if(! car_valid)
		{
			// delete this invalid car...
		}
	}
}

int Train::get_speed()
{
	return int(locomotives[0]->points[0]->velocity.length());
}

void Train::add_debug_car(float_32 x, float_32 y, Level* level)
{
	// deep copy old carriages to a bigger array
	Carriage** new_carriages = new Carriage* [num_car+1];
	for( int i = 0 ; i < num_car ; ++i ) {
		new_carriages[i] = carriages[i];
	}

	// add new carriage
	num_car++;
	new_carriages[num_car-1] = new Carriage(3.0f, x+30.0f, y-20.0f, level);

	delete carriages;
	carriages = new_carriages;
}


void Train::get_springs_to_p(const Particle* p, Spring*& s1, Spring*& s2) const
{
	s1 = nullptr;
	s2 = nullptr;
	// search all particles
	for(int i=0; i < num_loc; ++i)
	{
		for(int j=0; j < 5; ++j)
		{
			if(locomotives[i]->springs[j]->particles.first == p)
				s1 = locomotives[i]->springs[j];
			if(locomotives[i]->springs[j]->particles.second == p)
				s2 = locomotives[i]->springs[j];
		}
	}

	for(int i=0; i< num_car; ++i)
	{
		for(int j=0; j < 4; ++j)
		{
			if(carriages[i]->springs[j]->particles.first == p)
				s1 = carriages[i]->springs[j];
			if(carriages[i]->springs[j]->particles.second == p)
				s2 = carriages[i]->springs[j];
		}
	}
}


void Train::get_train_points_by_p(const Particle* p, Vector2d vertices[], int& vertices_num) const
{
	// search all particles
	vertices_num = 0;
	for(int i=0; i < num_loc && vertices_num == 0; ++i)
	{
		for(int j=0; j < 5 && vertices_num == 0; ++j)
		{
			if(locomotives[i]->springs[j]->particles.first == p || locomotives[i]->springs[j]->particles.second == p)
			{
				vertices_num = 5;
				for(int c=0; c < vertices_num; ++c)
				{
					vertices[c] = locomotives[i]->points[c]->pos;
				}
			}
		}
	}

	for(int i=0; i< num_car && vertices_num == 0; ++i)
	{
		for(int j=0; j < 4 && vertices_num == 0; ++j)
		{
			if(carriages[i]->springs[j]->particles.first == p || carriages[i]->springs[j]->particles.second == p)
			{
				vertices_num = 4;
				for(int c=0; c < vertices_num; ++c)
				{
					vertices[c] = carriages[i]->points[c]->pos;
				}
			}
		}
	}
}


