//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdio>
#include <iostream>
#include "gui_button.hpp"
#include "gui_manager.hpp"
#include "gui_editorsimulator.hpp"
#include "sdl_draw.hpp"
#include "vector2d.hpp"
#include "level.hpp"
#include "spring.hpp"
#include "particle.hpp"
#include "math_utils.hpp"
#include "gui_editor.hpp"
#include "settings.hpp"
#include "error.hpp"

GUI_Editor::GUI_Editor(const int level_id, const std::string& level_name, const std::string& level_full_path)
{
	int window_w = Settings::singleton()->window_width;
	int window_h = Settings::singleton()->window_height;

	sdl = Settings::singleton()->sdl_display;

	level = new Level(level_id, level_name, level_full_path);
	if( level->load_failed ) {
		ERROR_PRINT("failed to create level '%s' (id %d).\n", level_full_path.c_str(), level_id);
		// level creating failed
		error_button = new GUI_Button("Level Load Error", 200, 400, 600, 200, RES_INDEPENDENT);
		level->budget = 0;
	}
	else
	{
		sdl->center_level(level->width);
		INFO_PRINT("create level %d\n", level_id);
	}

	help_button = new GUI_Button("Help", 700, 0, 100, 30, RES_INDEPENDENT);
	buttons.push_back(help_button);

	run_button = new GUI_Button("Run", 900, 0, 100, 30, RES_INDEPENDENT);
	buttons.push_back(run_button);

	save_and_back_button = new GUI_Button("Back", 0, 0, 200, 30, RES_INDEPENDENT);
	buttons.push_back(save_and_back_button);

	back_button = new GUI_Button("Back without Save", 0, 40, 200, 30, RES_INDEPENDENT);
	buttons.push_back(back_button);

	clear_button = new GUI_Button("Clear Bridge", 0, 970, 200, 30, RES_INDEPENDENT);
	buttons.push_back(clear_button);

	undo_button = new GUI_Button("Undo", 0, 930, 100, 30, RES_INDEPENDENT);
	buttons.push_back(undo_button);

	redo_button = new GUI_Button("Redo", 100, 930, 100, 30, RES_INDEPENDENT);
	buttons.push_back(redo_button);

	char budget_all[20];
	std::snprintf(budget_all, 20, "Budget:%6d", level->budget);
	budget_button = new GUI_Button(budget_all, 0, window_h-160, 180, 25, LEFT_LOWER);
	budget_button->set_background(false);

	const int spring_cost = Settings::singleton()->spring_cost;
	std::snprintf(budget_str, 20, "used:  %7d", spring_cost * int(level->springs.size()));
	budget_used_button = new GUI_Button(budget_str, 0, window_h-130, 180, 25, LEFT_LOWER);
	budget_used_button->set_background(false);

	std::snprintf(budget_str, 20, "left:      %7d", 0 );
	budget_left_button = new GUI_Button(budget_str, 0, window_h-100, 180, 25, LEFT_LOWER);
	budget_left_button->set_background(false);

	level_name_button = new GUI_Button(level->get_name(), 300, 0, 400, 30, RES_INDEPENDENT);
	level_name_button->set_background(false);

	deck_button = new GUI_Button("Deck", 900, 100, 100, 30, RES_INDEPENDENT);
	buttons.push_back(deck_button);


	// Level Editor Buttons
	anchor_button = new GUI_Button("Anchor", 900, 650, 100, 30, RES_INDEPENDENT);
	buttons.push_back(anchor_button);

	water_button = new GUI_Button("Water", 900, 690, 100, 30, RES_INDEPENDENT);
	buttons.push_back(water_button);

	ground_button = new GUI_Button("Ground", 900, 730, 100, 30, RES_INDEPENDENT);
	buttons.push_back(ground_button);

	// Ground Editor Buttons
	level_width_plus_button = new GUI_Button("Level Width +", 800, 780, 200, 30, RES_INDEPENDENT);
	buttons.push_back(level_width_plus_button);

	level_width_minus_button = new GUI_Button("Level Width -", 800, 820, 200, 30, RES_INDEPENDENT);
	buttons.push_back(level_width_minus_button);

	level_shift_right_button = new GUI_Button("Shift Right", 800, 860, 200, 30, RES_INDEPENDENT);
	buttons.push_back(level_shift_right_button);

	level_shift_left_button = new GUI_Button("Shift Left", 800, 900, 200, 30, RES_INDEPENDENT);
	buttons.push_back(level_shift_left_button);


	max_stress_button = new GUI_Button("Max simulated Stress", 800, 970, 200, 30, RES_INDEPENDENT);
	buttons.push_back(max_stress_button);

	copy_button = new GUI_Button("Copy", 290, 970, 140, 30, RES_INDEPENDENT);
	buttons.push_back(copy_button);

	flip_x_button = new GUI_Button("Flip X", 430, 970, 140, 30, RES_INDEPENDENT);
	buttons.push_back(flip_x_button);

	delete_button = new GUI_Button("Delete", 570, 970, 140, 30, RES_INDEPENDENT);
	buttons.push_back(delete_button);

	first_level_tooltip_text = new GUI_Button("Click on a fixpoint (red) to create a beam.", window_w/2 -900/2, window_h/4, 900, 300, CENTER_UPPER);
	buttons.push_back(first_level_tooltip_text);
	//first_level_tooltip_text->set_background(false);
	first_level_tooltip_text->set_text_color(Settings::singleton()->sdlc_red);
	// do not push it into button list because it blocks real buttons (FIXME invisible buttons....)
	// buttons.push_back(first_level_tooltip_text);
	if(level->springs.size() > 0 || level->get_id() > 2 || level->id == 0)
	{
		first_level_tooltip_text->set_visible(false);
	}

	copy_paste_level_tooltip_text = new GUI_Button("New Feature unlocked: Copy Paste (activate with click and drag)", window_w/2 -900/2, window_h/4, 900, 300, CENTER_UPPER);
	buttons.push_back(copy_paste_level_tooltip_text);
	if( ! (level->id == 5 && level->springs.size() > 0) )
	{
		copy_paste_level_tooltip_text->set_visible(false);
	}

	deck_level_tooltip_text = new GUI_Button("New Material unlocked: Deck (activate with button)", window_w/2 -900/2, window_h/4, 900, 300, CENTER_UPPER);
	buttons.push_back(deck_level_tooltip_text);
	if( level->id == 0 || (! level->custom_deck) || level->id > 11 || level->springs.size() > 0 )
	{
		deck_level_tooltip_text->set_visible(false);
	}

	help_text = new GUI_Button("w,a,s,d to shift view; q,e to zoom; mouse to build and delete.", 10, 10, 980, 980, RES_INDEPENDENT);
	help_text->set_visible(false);
	buttons.push_back(help_text);

//	levelnum = levelnum_;
	new_particle_mouse = nullptr;
	new_particle_base = nullptr;
	new_spring = nullptr;

//	a_element_is_highlighted=false;
	highlighted_particle = nullptr;
	highlighted_spring = nullptr;


	current_editor_mode = REGULAR;
	cur_ground_index = 0;
}

GUI_Editor::~GUI_Editor()
{
	delete level;
	delete run_button;
	delete save_and_back_button;
	delete back_button;
	delete clear_button;
	delete undo_button;
	delete redo_button;
	delete level_name_button;
	if(level->load_failed) delete error_button;
	delete budget_left_button;
	delete budget_used_button;
	delete budget_button;

	delete ground_button;
	delete anchor_button;
	delete water_button;
	delete deck_button;
	delete max_stress_button;
	delete level_width_plus_button;
	delete level_width_minus_button;
	delete level_shift_left_button;
	delete level_shift_right_button;

	delete copy_button;
	delete delete_button;
	delete flip_x_button;

	delete first_level_tooltip_text;
	delete help_text;
	delete help_button;
}

// DRAW FUNCTIONS /////////////////////////////////////////////////////////////
void GUI_Editor::draw_all_particles() const
{
	// change spring/particle colors if in Water / Ground / Anchor editing mode
	SDL_Color pc;
	if(current_editor_mode == REGULAR || current_editor_mode == REGULAR_DECK ||
		current_editor_mode == REGULAR_COPY_MARK || current_editor_mode == REGULAR_COPY_MOVE	)
		pc = Settings::singleton()->sdlc_particle;
	else
		pc = Settings::singleton()->sdlc_particle_inactive;

	SDL_Color pfc = Settings::singleton()->sdlc_particle_fixed;
	float_32 particle_radius_edit = Settings::singleton()->particle_radius_edit * float_32( Settings::singleton()->window_height);
	for (Particle_iter i = level->particles.begin(); i != level->particles.end(); ++i)
	{
		if((*i)->fixed)
			sdl->draw_particle((*i)->pos.x, (*i)->pos.y, particle_radius_edit, pfc);
		else
			sdl->draw_particle((*i)->pos.x, (*i)->pos.y, particle_radius_edit, pc);
    }
	if( highlighted_particle != nullptr)
	{
		SDL_Color hlpc = Settings::singleton()->sdlc_particle_highlight;
		sdl->draw_particle(highlighted_particle->pos.x, highlighted_particle->pos.y, particle_radius_edit, hlpc);
	}

	// draw current edit particle
	//   invisible if: longer max_spring_len, with endpoint inside ground)
	if(new_particle_mouse != nullptr)
	{
		if(level->budget_left < 0 )
			pc = Settings::singleton()->sdlc_red;

		sdl->draw_particle(new_particle_mouse->pos.x, new_particle_mouse->pos.y, particle_radius_edit, pc);
	}

	// draw copied springs
	if(current_editor_mode == REGULAR_COPY_MARK || current_editor_mode == REGULAR_COPY_MOVE)
	{
		SDL_Color hlpc = Settings::singleton()->sdlc_particle_highlight;
		Particle* p_ptr;
		for (size_t i = 0; i < level->copy_particles.size(); ++i)
		{
			p_ptr = level->copy_particles[i];
			sdl->draw_particle(p_ptr->pos.x, p_ptr->pos.y, particle_radius_edit, hlpc);
		}
	}
}

void GUI_Editor::draw_all_springs() const
{
	SDL_Color dc, sc, color;
	float_32 line_width = 8;

	// change spring/particle colors if in Water / Ground / Anchor editing mode
	if(current_editor_mode == REGULAR || current_editor_mode == REGULAR_DECK ||
		current_editor_mode == REGULAR_COPY_MARK || current_editor_mode == REGULAR_COPY_MOVE	)
	{
		dc = Settings::singleton()->sdlc_deck;
		sc = Settings::singleton()->sdlc_spring;
	}
	else
	{
		dc = Settings::singleton()->sdlc_deck_inactive;
		sc = Settings::singleton()->sdlc_spring_inactive;
	}

	float_32 spring_line_width_edit = Settings::singleton()->spring_line_width_edit;
	float_32 spring_deck_line_width_edit = Settings::singleton()->spring_deck_line_width_edit;


	// draw all springs
	for (Spring_iter i = level->springs.begin(); i != level->springs.end(); ++i)
	{
		// if max stress is enabled
		if(display_max_stress)
		{
			float_32 stress_color = (*i)->get_max_color();
			if (stress_color > 0.0f)
			{
				// pull color
				color = {0, Uint8((1.0f - stress_color)*255.0f), Uint8(stress_color*255.0f), 255};
			}
			else
			{
				// push color
				color = {Uint8(-stress_color*255.0f), Uint8((1.0f + stress_color)*255.0f), 0, 255};
			}
			line_width = Settings::singleton()->spring_line_width_max_stress_edit;
		}
		else
		{
			if( (*i)->type == Spring::DECK )
			{
				line_width = spring_deck_line_width_edit;
				color = dc;
			}
			else
			{
				line_width = spring_line_width_edit;
				color = sc;
			}
		}

		sdl->draw_spring( (*i)->particles.first->pos.x, (*i)->particles.first->pos.y,
						(*i)->particles.second->pos.x, (*i)->particles.second->pos.y,
						line_width, color );
	}

	// mouse passive motion highlight
	if( highlighted_spring != nullptr )
	{
		if(highlighted_spring->type == Spring::DECK)
			line_width = spring_deck_line_width_edit;
		else
			line_width = spring_line_width_edit;

		sdl->draw_spring(highlighted_spring->particles.first->pos.x, highlighted_spring->particles.first->pos.y,
						highlighted_spring->particles.second->pos.x, highlighted_spring->particles.second->pos.y,
						line_width, Settings::singleton()->sdlc_spring_highlight );
	}

	// draw current edit spring
	//   invisible if: longer max_spring_len, with endpoint inside ground)
	if(new_spring != nullptr && new_spring_visible)
	{
		line_width = spring_line_width_edit;
		SDL_Color color = Settings::singleton()->sdlc_spring ;
		if(( !level->custom_deck && new_spring->particles.first->pos.y == 0 &&  new_spring->particles.second->pos.y == 0) ||
				current_editor_mode == REGULAR_DECK)
		{
			line_width = spring_deck_line_width_edit;
			color = Settings::singleton()->sdlc_deck;
		}
		if(level->budget_left < 0)
			color = Settings::singleton()->sdlc_red;
		// draw
		sdl->draw_spring(new_spring->particles.first->pos.x, new_spring->particles.first->pos.y,
			new_spring->particles.second->pos.x, new_spring->particles.second->pos.y,
			line_width, color);
	}

	// draw copied springs
	if(current_editor_mode == REGULAR_COPY_MARK || current_editor_mode == REGULAR_COPY_MOVE)
	{
		Particle* p_first;
		Particle* p_second;
		for (size_t i = 0; i < level->copy_springs.size(); ++i)
		{
			p_first = level->copy_springs[i]->particles.first;
			p_second = level->copy_springs[i]->particles.second;

			sdl->draw_spring(p_first->pos.x, p_first->pos.y, p_second->pos.x, p_second->pos.y,
					line_width, Settings::singleton()->sdlc_spring_highlight );
		}
	}
}

void GUI_Editor::draw_all_buttons() const
{
	// FIXME: draw all gui components (loop) but set them (in)visible
	sdl->draw_button(first_level_tooltip_text);
	sdl->draw_button(copy_paste_level_tooltip_text);
	sdl->draw_button(deck_level_tooltip_text);
#ifndef DEBUG
	if(level->editable)
#endif
	{
		if(current_editor_mode == GROUND) {
			ground_button->set_highlight( true );

			// draw level expand / shrink / shift buttons
			sdl->draw_button(level_width_plus_button);
			sdl->draw_button(level_width_minus_button);
			sdl->draw_button(level_shift_right_button);
			sdl->draw_button(level_shift_left_button);
		}
		else {
			ground_button->set_highlight( false );
		}
		sdl->draw_button(ground_button);

		if(current_editor_mode == ANCHOR)
			anchor_button->set_highlight(true);
		else
			anchor_button->set_highlight(false);
		sdl->draw_button(anchor_button);

		if(current_editor_mode == WATER)
			water_button->set_highlight(true);
		else
			water_button->set_highlight(false);
		sdl->draw_button(water_button);
	}
	if(level->custom_deck)
	{
		if(current_editor_mode == REGULAR_DECK)
			deck_button->set_highlight(true);
		else
			deck_button->set_highlight(false);
		sdl->draw_button(deck_button);
	}

	sdl->draw_button(run_button);
	sdl->draw_button(level_name_button);
	sdl->draw_button(save_and_back_button);
	sdl->draw_button(back_button);
	sdl->draw_button(clear_button);
	//sdl->draw_button(undo_button);
	//sdl->draw_button(redo_button);
	sdl->draw_button(budget_left_button);
	sdl->draw_button(budget_used_button);
	sdl->draw_button(budget_button);
	sdl->draw_button(help_text);
	sdl->draw_button(help_button);

	if(display_max_stress)
		max_stress_button->set_highlight(true);
	else
		max_stress_button->set_highlight(false);
	sdl->draw_button(max_stress_button);

	if(current_editor_mode == REGULAR_COPY_MARK)
	{
		sdl->draw_button(delete_button);
	}
	if(current_editor_mode == REGULAR_COPY_MARK || current_editor_mode == REGULAR_COPY_MOVE  )
	{
		if(level->copy_particles.size() > 0)
		{
			sdl->draw_button(copy_button);
			sdl->draw_button(flip_x_button);
		}
	}

	if( level->load_failed ) {
		sdl->draw_button(error_button);
	}
}

void GUI_Editor::draw() const
{
	sdl->draw_background(Settings::singleton()->sdlc_background_editor);

	sdl->draw_grid(Settings::singleton()->sdlc_grid_most, Settings::singleton()->sdlc_grid_some);

	if(display_max_stress) // water makes it difficult to distinct colors so change draw order
	{
		sdl->draw_water(level->water_height, Settings::singleton()->sdlc_water);
		this->draw_all_springs();
	}
	else
	{
		this->draw_all_springs();
		sdl->draw_water(level->water_height, Settings::singleton()->sdlc_water);
	}

	if(! level->load_failed )
		sdl->draw_ground(level->ground, level->width, Settings::singleton()->sdlc_ground);

	this->draw_all_particles();

	if(current_editor_mode == REGULAR_COPY_MARK)
	{
		const SDL_Color c = {255, 255, 255, 60};
		sdl->draw_rect(mark_rect_x, mark_rect_y, mark_rect_w, mark_rect_h, c);
	}

#ifndef DEBUG
	if(level->editable)
#endif
	{
		if(current_editor_mode == GROUND)
		{
			sdl->draw_ground_editbox((float_32) cur_ground_index * 5.0f - 2.5f, 5.0f, level->ground[cur_ground_index],
					Settings::singleton()->sdlc_ground_edit_box,
					Settings::singleton()->sdlc_ground_edit_box_hl );

			// draw level 0 x and max x lines
			SDL_Color line_color = {60,60,255,200};
			sdl->draw_spring(0, sdl->screen_to_world_y(0), 0, -200.0f, 1, line_color);
			sdl->draw_spring((float_32)level->width, sdl->screen_to_world_y(0), (float_32) level->width, -200.0f, 1, line_color);
		}
	}

	this->draw_all_buttons();

#ifdef DEBUG
	// draw paricle properties under coursor
	if(current_editor_mode == REGULAR){
		const float_32 mouse_w_x = sdl->screen_to_world_x(Settings::singleton()->mouse_x);
		const float_32 mouse_w_y = sdl->screen_to_world_y(Settings::singleton()->mouse_y);

		const Vector2d mouse_pos = Vector2d(mouse_w_x, mouse_w_y);
		Particle* nearest_particle;
		if( (nearest_particle = level->get_nearest_particle(mouse_pos, level->particles)) != nullptr)
		{
			if(Vector2d::distance_square( mouse_pos, nearest_particle->pos) < 5 * 5)
			{
				const SDL_Color debug_color = {255, 255, 255, 254};
				char particle_id[20];
				std::snprintf(particle_id, 20, "id: %d", nearest_particle->id);
				sdl->draw_string(Settings::singleton()->mouse_x + 10 ,Settings::singleton()->mouse_y - 10, particle_id, debug_color);
				char links[20];
				std::snprintf(links, 20, "links: %d", nearest_particle->spring_links);
				sdl->draw_string(Settings::singleton()->mouse_x + 10 ,Settings::singleton()->mouse_y + 5, links, debug_color);
			}
		}
	}

	// debug footer line (alpha 254 to prevent strange SDL flicker
	SDL_Color debug_color = {255, 255, 255, 254};
	sdl->draw_string(250.0f, Settings::singleton()->window_height - 9, editor_footer, debug_color);
#endif
}

// UPDATE FUNCTIONS ////////////////////////////////////////////////////////////
void GUI_Editor::update_budget()
{
	// recalc budget
	const int spring_cost = Settings::singleton()->spring_cost;

	// test if current budget left is different from last one
	int tmp_budget = level->budget - spring_cost * int(level->springs.size()) - spring_cost * copy_mode_springs_num;
	// while drawing a spring, add it
	if(new_spring != nullptr)
		tmp_budget -= spring_cost;

	// do the test
	if(tmp_budget != level->budget_left)
	{
		if(tmp_budget < 0 )
		{
			budget_left_button->set_text_color(Settings::singleton()->sdlc_red);
			budget_used_button->set_text_color(Settings::singleton()->sdlc_red);
		}
		else
		{
			budget_left_button->set_text_color(Settings::singleton()->sdlc_white);
			budget_used_button->set_text_color(Settings::singleton()->sdlc_white);
		}

		// redraw budget left button
		std::snprintf(budget_str, 20, "left:      %7d", tmp_budget);
		budget_left_button->set_new_text(budget_str);
		level->budget_left = tmp_budget;

		// redraw budget used button
		std::snprintf(budget_str, 20, "used:    %7d", spring_cost * int(level->springs.size()) + spring_cost * copy_mode_springs_num);
		budget_used_button->set_new_text(budget_str);
	}
}

void GUI_Editor::update()
{
	this->update_budget();

	// make tooltip in level 1 invisible if first beam was created
	if(level->springs.size() > 0)
	{
		first_level_tooltip_text->set_visible(false);
		copy_paste_level_tooltip_text->set_visible(false);
		deck_level_tooltip_text->set_visible(false);
	}

	switch(current_editor_mode)
	{
		case REGULAR:
		case REGULAR_DECK:
			// search for spring and particles to highlight and draw it in ::draw()
			this->passive_mouse_motion();
			break;
		case REGULAR_COPY_MOVE:
			// bool place_it = false; // only move it around
			this->copy_mode_mouse_motion_move(false);
			break;
		case REGULAR_COPY_MARK:
			this->copy_mode_mouse_motion();
			break;
		case GROUND: {
				int mx_screen = Settings::singleton()->mouse_x;
				int my_screen = Settings::singleton()->mouse_y;
				float_32 mx_world = sdl->screen_to_world_x(mx_screen);
				float_32 my_world = sdl->screen_to_world_y(my_screen);
				// check if mouse is still on a button
				GUI_Component* component = 0;
				for (Component_List::iterator i = buttons.begin (); i != buttons.end (); ++i)
				{
					if((*i)->is_at(mx_screen, my_screen) && (*i)->get_visible())
						component = *i;
				}
				if(component && component->get_visible())
					return;

				int xr = Math_utils::round_to(mx_world, 5) / 5 ;
				xr = level->fit_ground_index_to_boundaries(xr);
				if(cur_ground_index != xr)
				{
					cur_ground_index = xr;
					// not allow to edit first or last ground (due to bug in draw functions)
					if(cur_ground_index == 0) cur_ground_index =1;
					if(cur_ground_index == level->ground_len-1) cur_ground_index = level->ground_len-2;
				}
				// if mouse left down set ground height
				//
				if( Settings::singleton()->mouse_left_down )
					level->ground[cur_ground_index] = std::round(my_world);

				// if mouse right down set ground height = 0
				if( Settings::singleton()->mouse_right_down )
					level->ground[cur_ground_index] = 0;
			}
			break;
		case ANCHOR: break;
		case WATER: break;
	}

#ifdef DEBUG
	std::snprintf(editor_footer, EDITOR_FOOTER_SIZE, "EDT PNum: %5zu, SNum: %5zu",
		level->particles.size(), level->springs.size());
#endif
}

// EVENT FUNCTIONS ////////////////////////////////////////////////////////////
void GUI_Editor::event_keyboard(GUI_Manager* guimanager, const char* utf8_key)
{
	GUI_Editorsimulator::event_keyboard(utf8_key);
	std::string utf8_string = utf8_key;

	// short key "r" for run simulation
	if(utf8_string.compare("r") == 0 && ! level->load_failed)
	{
		// switch to simulator
		this->clear_mouse_state();
		this->update_budget();
		level->copy_for_simulation();
		guimanager->createGUI_Simulator(level);
		guimanager->currentGUI = guimanager->GUISIMULATOR;
		// maybe window was resized, so object positions (buttons)
		guimanager->event_resize();
	}
}

void GUI_Editor::event_mouse_click(GUI_Manager* guimanager, int button, int button_state, int x, int y)
{
	// button down event
	if(!button_state)
	{
		// click on a button?
		GUI_Component* component = nullptr;
		for(Component_List::iterator i = buttons.begin (); i != buttons.end (); ++i)
		{
			if((*i)->is_at(x, y) && (*i)->get_visible())
				component = *i;
		}
		if(component==save_and_back_button && button==0) // left click (first mouse button)
		{
			this->clear_mouse_state();
			if(! level->load_failed ) level->save_to_file();
			guimanager->currentGUI = guimanager->GUIMENU_LEVELS;
			// maybe window was resized, so object positions (buttons)
			guimanager->event_resize();
		}
		if(component==back_button && button==0) // left click (first mouse button)
		{
			this->clear_mouse_state();
			guimanager->currentGUI = guimanager->GUIMENU_LEVELS;
			// maybe window was resized, so object positions (buttons)
			guimanager->event_resize();
		}
		if(component==clear_button && button==0) // left click (first mouse button)
		{
			this->clear_mouse_state();
			this->clear_bridge();
		}
// Upcoming Feature: Undo & Redo
//		if(component==undo_button && button==0) // left click (first mouse button)
//		{
//			this->clear_mouse_state();
//			level = undomanager.undo(level);
//		}
//		if(component==redo_button && button==0) // left click (first mouse button)
//		{
//			this->clear_mouse_state();
//			level = undomanager.redo(level);
//		}

		if(component==run_button && button==0 && ! level->load_failed) // left click (first mouse button)
		{
			// switch to simulator
			// call short key function
			this->event_keyboard(guimanager, "r");
		}
		if(component==max_stress_button && button==0) // left click (first mouse button)
		{
			this->clear_mouse_state();
			display_max_stress = !display_max_stress;
		}
		if(component==first_level_tooltip_text && button==0 && first_level_tooltip_text->get_visible()) // left click (first mouse button)
			first_level_tooltip_text->set_visible(false);
		if(component==copy_paste_level_tooltip_text && button==0 && copy_paste_level_tooltip_text->get_visible()) // left click (first mouse button)
			copy_paste_level_tooltip_text->set_visible(false);
		if(component==deck_level_tooltip_text && button==0 && deck_level_tooltip_text->get_visible()) // left click (first mouse button)
			deck_level_tooltip_text->set_visible(false);
		if(component==help_text && button==0 && help_text->get_visible()) // left click (first mouse button)
		{
			this->clear_mouse_state();
			help_text->set_visible(false);
		}
		if(component==help_button && button==0) // left click (first mouse button)
		{
			this->clear_mouse_state();
			help_text->set_visible(true);
		}
#ifndef DEBUG
		if(level->editable)
#endif
		{
			if(component==ground_button && button==0) // left click (first mouse button)
			{
				this->clear_mouse_state();
				if(current_editor_mode != GROUND){
					current_editor_mode = GROUND;

					int xr = Math_utils::round_to(float_32(Settings::singleton()->mouse_x), 5) / 5 ;
					xr = level->fit_ground_index_to_boundaries(xr);
					if(cur_ground_index != xr)
					{
						cur_ground_index = xr;
						// due to bug (in draw SDL function)
						if(cur_ground_index == 0) cur_ground_index =1;
						if(cur_ground_index == level->ground_len-1) cur_ground_index = level->ground_len-2;
					}
				}
				else current_editor_mode = REGULAR;
			}
			if(component==anchor_button && button==0) // left click (first mouse button)
			{
				this->clear_mouse_state();
				if(current_editor_mode != ANCHOR) current_editor_mode = ANCHOR;
				else current_editor_mode = REGULAR;
			}
			if(component==water_button && button==0) // left click (first mouse button)
			{
				this->clear_mouse_state();
				if(current_editor_mode != WATER) current_editor_mode = WATER;
				else current_editor_mode = REGULAR;
			}
			if(current_editor_mode == GROUND)
			{
				if(component==level_width_plus_button && button==0) // left click (first mouse button)
				{
					this->clear_mouse_state();
					level->adjust_level_width(80);
				}
				if(component==level_width_minus_button && button==0) // left click (first mouse button)
				{
					this->clear_mouse_state();
					level->adjust_level_width(-80);
				}
				if(component==level_shift_right_button && button==0)
				{
					this->clear_mouse_state();
					level->level_shift_left(-10);
				}
				if(component==level_shift_left_button && button==0)
				{
					this->clear_mouse_state();
					level->level_shift_left(10);
				}
			}
		} // end level->editable buttons
		// avoid invisible button (missed click because of button is in between)
		if((component==level_width_plus_button || component==level_width_minus_button || component==level_shift_right_button || component==level_shift_left_button) && current_editor_mode != GROUND )
				component = nullptr;
		if(level->custom_deck)
		{
			if(component==deck_button && button==0) // left click (first mouse button)
			{
				this->clear_mouse_state();
				if(current_editor_mode != REGULAR_DECK) current_editor_mode = REGULAR_DECK;
				else current_editor_mode = REGULAR;
			}
		}

		if((component==copy_button || component==flip_x_button || component==delete_button) && button==0) // left click (first mouse button)
		{
			if(current_editor_mode == REGULAR_COPY_MARK && component==delete_button)
			{
				// delete copied particles and springs
				level->free_copied_particles_and_springs();
				level->delete_springs_from_area(mark_rect_x, mark_rect_y, mark_rect_w, mark_rect_h);
				current_editor_mode = REGULAR;
			}
			if(current_editor_mode == REGULAR_COPY_MARK && component==copy_button)
			{
				// move copied particles and springs with mouse pointer
				current_editor_mode = REGULAR_COPY_MOVE;
			}
			if(current_editor_mode == REGULAR_COPY_MOVE && component==flip_x_button)
			{
				level->flip_x_copied_particles_and_springs();
			}
			if(current_editor_mode != REGULAR_COPY_MOVE && current_editor_mode != REGULAR_COPY_MARK)
				component = nullptr;
		}
		else
		{
			// any other button will end copy & paste mode
			if(current_editor_mode == REGULAR_COPY_MARK)
			{
				level->free_copied_particles_and_springs();
				current_editor_mode = REGULAR;
			}
		}

		if(component==nullptr) // it was not a button // FIXME ghostbuttons
		{
			if(button==0) this->event_mouse_left_down(sdl->screen_to_world_x(x), sdl->screen_to_world_y(y));
			//if(button==1) event_mouse_middle_down(x, y);
			if(button==2) this->event_mouse_right_down(sdl->screen_to_world_x(x), sdl->screen_to_world_y(y));
		}
	}
}

void GUI_Editor::event_mouse_left_down(float_32 x, float_32 y)
{
	// FIXME use switch case here...
	if(current_editor_mode == REGULAR_COPY_MOVE)
	{
		// place copied selection at current location
		bool place_it = true;
		copy_mode_mouse_motion_move(place_it);
	}
	if(current_editor_mode == ANCHOR)
	{
		Particle* testp = this->get_particle_by_xy( x, y);
		if(testp) // Particle under courser
		{
			Vector2d pos(x, y);
			testp->fixed = true;
		}
		else // no Particle under courser; create one
		{
			int xr = Math_utils::round_to(x, 10);
			int yr = Math_utils::round_to(y, 10);
			Vector2d pos((float_32) xr, (float_32) yr);
			const bool fixed = true;
			level->add_particle(pos, particle_mass, fixed);
		}
	}
	else if(current_editor_mode == GROUND)
	{
		level->ground[cur_ground_index] = (float_32) Math_utils::round(y);
	}
	else if(current_editor_mode == WATER)
	{
		level->water_height = (float_32) Math_utils::round(y);
	}
	else if(current_editor_mode == REGULAR || current_editor_mode == REGULAR_DECK)
	{
		Vector2d pos(float_32( Math_utils::round_to(x, 10)), float_32( Math_utils::round_to(y, 10)) );

		// get spring properties
		const float_32 s_stiffness = Settings::singleton()->spring_stiffness;
		const float_32 s_damping = Settings::singleton()->spring_damping;
		const float_32 s_max_stretch = Settings::singleton()->spring_max_stretch;

		if(new_particle_base == nullptr)
		{	// nothing selected yet
			new_particle_base = this->get_particle_by_xy(pos.x, pos.y);
			// is particle under cursor?
			if(new_particle_base)
			{	// clicked on a particle

				// new particle to draw a spring to current cusor position
				const bool fixed = false;
				const int fake_id = -1;
				new_particle_mouse = new Particle(fake_id, pos, particle_mass, fixed, false);
				if(current_editor_mode == REGULAR_DECK)
					new_spring = new Spring(new_particle_mouse, new_particle_base, s_stiffness, s_damping, s_max_stretch, Spring::DECK );
				else
					new_spring = new Spring(new_particle_mouse, new_particle_base, s_stiffness, s_damping, s_max_stretch, Spring::BRIDGE );
			}
			else
			{
#ifndef DEBUG
				// do not confuse users before level 8 with mark & copy
				if(level->id >= Settings::singleton()->copy_paste_in_editor_threshold || level->id == 0)
#endif
				{
					current_editor_mode = REGULAR_COPY_MARK;
					draw_copy_rect_with_mouse = true;
					mark_rect_x = x;
					mark_rect_y = y;
				}
			}
		}
		else
		{
			// ABORT if:
			// spring is invisible (because it is too long or with particle inside the ground, point is in line)
			// OR budget is excited
			// OR particle is on an existing spring (only if new particle)
			if( (!new_spring_visible) ||  level->budget_left < 0)
			{
				this->clear_mouse_state();
			}
			else // spring is ok
			{
				// create new particle
				const bool fixed = false;
				Particle* tmp_particle = level->add_particle(new_particle_mouse->pos, particle_mass, fixed);
				delete new_particle_mouse;
				new_particle_mouse = nullptr;
				delete new_spring;
				new_spring = nullptr;

				// create new spring with new target particle
				if( (!level->custom_deck && new_particle_base->pos.y == 0 && tmp_particle->pos.y == 0) ||
						current_editor_mode == REGULAR_DECK)
					level->add_spring(new_particle_base, tmp_particle, s_stiffness, s_damping, s_max_stretch, Spring::DECK );
				else
					level->add_spring(new_particle_base, tmp_particle, s_stiffness, s_damping, s_max_stretch, Spring::BRIDGE );
				new_particle_base = tmp_particle;

				// new particle to draw a spring to current cusor position
				const int fake_id = -1;
				new_particle_mouse = new Particle(fake_id, pos, particle_mass, fixed, false);
				if(current_editor_mode == REGULAR_DECK)
					new_spring = new Spring(new_particle_mouse, new_particle_base, s_stiffness, s_damping, s_max_stretch, Spring::DECK );
				else
					new_spring = new Spring(new_particle_mouse, new_particle_base, s_stiffness, s_damping, s_max_stretch, Spring::BRIDGE );
			}
		}
	}
}

void GUI_Editor::event_mouse_right_down(float_32 x, float_32 y)
{
	if(current_editor_mode == REGULAR_COPY_MOVE)
	{
		level->free_copied_particles_and_springs();
		copy_mode_springs_num = 0;
		current_editor_mode = REGULAR;
	}
	if(current_editor_mode == ANCHOR)
	{
		Particle* testp = this->get_particle_by_xy( x, y);
		if(testp && testp->fixed == true) // Particle under courser
		{
			bool force = true;
			level->delete_particle(testp, force);
		}
	}
	else if(current_editor_mode == GROUND)
	{
		level->ground[cur_ground_index] = 0;
	}
	else if(current_editor_mode == REGULAR || current_editor_mode == REGULAR_DECK)
	{
		if(new_particle_base)
		{
			this->clear_mouse_state();
		}
		else
		{
			Particle* particle = this->get_particle_by_xy (x, y);
			if (particle)
			{
				level->delete_particle(particle);
			}
			else // no Particle is near
			{
				Spring* spring = this->get_spring_by_xy (x, y);
				if(spring)
				{
					level->delete_spring(spring);
				}
			}
		}
	}
}

void GUI_Editor::event_window_resize()
{
	for (Component_List::iterator i = buttons.begin (); i != buttons.end (); ++i)
	{
		(*i)->on_window_resize();
	}
	budget_button->on_window_resize();
	budget_left_button->on_window_resize();
	budget_used_button->on_window_resize();
	level_name_button->on_window_resize();
}

// HELPER FUNCTIONS ////////////////////////////////////////////////////////////
void GUI_Editor::clear_mouse_state()
{
	if(new_spring != nullptr)
		delete new_spring;
	new_spring = nullptr;
	new_particle_mouse = nullptr;
	new_particle_base = nullptr;
}

void GUI_Editor::copy_mode_mouse_motion()
{
	if(draw_copy_rect_with_mouse)
	{
		// while drag the copyrect update it
		float_32 x = sdl->screen_to_world_x(Settings::singleton()->mouse_x);
		float_32 y = sdl->screen_to_world_y(Settings::singleton()->mouse_y);

		mark_rect_w = x - mark_rect_x;
		mark_rect_h = y - mark_rect_y;

		if(! Settings::singleton()->mouse_left_down)
		{
			// mark mode ended: find springs and particles inside mark rect
			level->copy_particles_and_springs_inside_rect(mark_rect_x, mark_rect_y, mark_rect_w, mark_rect_h);
			draw_copy_rect_with_mouse = false;
		}
	}
	else
	{
		// made only a click but nearly no drag to mark springs and particles
		// => go back to REGULAR Mode
		if(std::fabs(mark_rect_w) < 10 && std::fabs(mark_rect_h) < 10)
			current_editor_mode = REGULAR;
	}
}

void GUI_Editor::copy_mode_mouse_motion_move(const bool place_it)
{
	float_32 x = sdl->screen_to_world_x(Settings::singleton()->mouse_x);
	float_32 y = sdl->screen_to_world_y(Settings::singleton()->mouse_y);

	// snap to grid
	float_32 xr = (float_32) Math_utils::round_to(x, 10);
	float_32 yr = (float_32) Math_utils::round_to(y, 10);

	level->move_copied_particles_and_springs_to(xr, yr);
	copy_mode_springs_num = level->get_new_springs_num(xr, yr);
	this->update_budget();
	if(place_it && level->budget_left >= 0)
	{
		level->add_copied_particles_and_springs();
		//level->free_copied_particles_and_springs();
		//current_editor_mode = REGULAR;
	}
}

void GUI_Editor::passive_mouse_motion()
{
	const int grid_spacing = Settings::singleton()->grid_spacing;
	float_32 x = sdl->screen_to_world_x(Settings::singleton()->mouse_x);
	float_32 y = sdl->screen_to_world_y(Settings::singleton()->mouse_y);
	// move particle with cursor (creating new spring)
	if(new_particle_mouse != nullptr)
	{
		// snap to grid
		int xr = Math_utils::round_to(x, grid_spacing);
		int yr = Math_utils::round_to(y, grid_spacing);

		Vector2d mouse_r_vec((float_32) xr, (float_32) yr);
		Vector2d mouse_vec(x, y);
		const float_32 max_spring_len_sq = Settings::singleton()->max_spring_len * Settings::singleton()->max_spring_len;
		// if spring is too long, make spring max length
		if( Vector2d::distance_square(mouse_r_vec, new_particle_base->pos) > max_spring_len_sq)
		{
			Vector2d direction_vec = mouse_vec - new_particle_base->pos;
			direction_vec.normalize();
			Vector2d longest_allowed_spring_vec = direction_vec * Settings::singleton()->max_spring_len;
			longest_allowed_spring_vec -= direction_vec * 0.5f * std::sqrt(float_32(grid_spacing * grid_spacing));

			xr = Math_utils::round_to(new_particle_base->pos.x + longest_allowed_spring_vec.x, grid_spacing);
			yr = Math_utils::round_to(new_particle_base->pos.y + longest_allowed_spring_vec.y, grid_spacing);
		}

		if(xr != (int) new_particle_mouse->pos.x || yr != (int) new_particle_mouse->pos.y)
		{
			highlighted_particle = nullptr;
			new_particle_mouse->pos.x = (float_32) xr;
			new_particle_mouse->pos.y = (float_32) yr;
			new_spring->recalc_length();


			// make spring not visible if it is too long or inside ground
			// (but allow fixed particles to be very slightly inside ground)
			bool mouse_inside_ground = level->p_inside_ground(new_particle_mouse->pos);
			Particle* p_mouse = this->get_particle_by_xy(new_particle_mouse->pos.x, new_particle_mouse->pos.y);
			bool p_mouse_fixed = false;
			if(p_mouse != nullptr && p_mouse->fixed)
				p_mouse_fixed = true;

			if(new_spring->length > Settings::singleton()->max_spring_len ||
						(mouse_inside_ground && !p_mouse_fixed)
						|| level->on_spring_line(new_particle_mouse->pos))
			{
					new_spring_visible = false;
			}
			else
			{
				new_spring_visible = true;
			}
        }
	}
	else // search for objects to highlight under cursor
	{
		Particle* search_particle = this->get_particle_by_xy (x, y);
		if (search_particle != nullptr) {
			if( highlighted_particle == nullptr || highlighted_particle != search_particle || highlighted_spring != nullptr ) {
				// remember adress for later draw
				highlighted_particle = search_particle;
				highlighted_spring = nullptr;
			}
		}
		else // no Particle is near
		{
			Spring* search_spring = this->get_spring_by_xy (x, y);
			if(search_spring != nullptr)
			{
				if( highlighted_spring == nullptr || highlighted_spring != search_spring || highlighted_particle != nullptr )
				{
					highlighted_spring = search_spring;
					highlighted_particle = nullptr;
				}
			}
			else // mouse is not over something
			{
				highlighted_spring = nullptr;
				highlighted_particle = nullptr;
			}
		}
	}
}

Spring* GUI_Editor::get_spring_by_xy(float_32 x, float_32 y)
{
	Spring* spring = 0;
	float_32 min_distance = 0.0f;

	float_32 capture_threshold = 5;

	for (Spring_iter i = level->springs.begin (); i != level->springs.end (); ++i)
	{
		float_32 x0 = x;
		float_32 y0 = y;
		float_32& x1 = (*i)->particles.first->pos.x;
		float_32& y1 = (*i)->particles.first->pos.y;
		float_32& x2 = (*i)->particles.second->pos.x;
		float_32& y2 = (*i)->particles.second->pos.y;

		// can be optimized
		float_32 u = (((x0 - x1)*(x2-x1) + (y0 - y1)*(y2 - y1))
		/ ((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)));

		float_32 distance = (float_32) (fabs((x2 - x1)*(y1-y0) - (x1-x0)*(y2-y1))
		/ sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1)));

		if (u >= 0 && u <= 1.0f
			&& ((spring && min_distance > distance)
			|| (!spring && distance <= capture_threshold))) // FIXME: threashold is dependend on view
		{
			spring = *i;
			min_distance = distance;
		}
	}

	return spring;
}

Particle* GUI_Editor::get_particle_by_xy(float_32 x, float_32 y, const float_32 catch_distance)
{
	Particle* particle = nullptr;
	Vector2d mouse_pos (x, y);

	for (Particle_iter i = level->particles.begin (); i != level->particles.end (); ++i)
	{
		Vector2d diff = mouse_pos - (*i)->pos;

												// avoid getting the new_particle_mouse FIXME (not needed)
		if (diff.length_square() < catch_distance*catch_distance && *i != new_particle_mouse)
		{
			particle = *i;
		}
	}
	return particle;
}

void GUI_Editor::clear_bridge()
{
	undomanager.save_current_state(level);
	level->clear_bridge();
}

void GUI_Editor::user_pressed_help()
{
	if(help_text->get_visible())
		help_text->set_visible(false);
	else
		help_text->set_visible(true);
}

void GUI_Editor::draw_help() const
{
	sdl->draw_button(help_text);
}
