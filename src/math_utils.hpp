//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_MATH_UTILS_H
#define HEADER_MATH_UTILS_H

class Particle;
class Spring;

namespace Math_utils{

inline int round(const float_32 a) {
	return int((a > 0) ? (a + .5f) : (a - .5));
}

inline int round_to(const float_32 x,const int n) {
	if (x > 0)
		return (((int)(x + (float_32) (n/2))) / n) * n;
	else
		return (((int)(x - (float_32) (n/2))) / n) * n;
}

inline int max(const int a,const int b) {
	return a > b ? a : b;
}

inline int min(const int a,const int b) {
	return a < b ? a : b;
}

inline bool is_equal(const float_32 a, const float_32 b, const float_32 maxdist) {
	return a + maxdist > b && a - maxdist < b;
}

/// simplified version of line segment intersection test with orientation, see:
/// http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
uint_fast8_t orientation(const Vector2d& p, const Vector2d& q, const Vector2d& r);

// This function returns true if line segment 's1s2'
// and 't1t2' intersect.
bool line_segments_cross(const Vector2d& s1, const Vector2d& s2, const Vector2d& t1, const Vector2d& t2);
bool line_segment_polygon_cross(const Vector2d& s1, const Vector2d& s2, const Vector2d* vertices, const int vertices_num);

Vector2d center_of_polygon(const Vector2d* vertices, const int vertices_num);

// Doxygen Kommentar {{{
/** checks if a point is below a give line.
 * A o
 *    \ o P
 *     \
 *      \
 *       o B
 */
// }}}
bool point_below_line_segment(const float_32 px, const float_32 py, const float_32 ax, const float_32 ay, const float_32 bx, const float_32 by);

bool point_below_line(const Vector2d& p, const Vector2d& a, const Vector2d& b);
/**
 * check if a point is inside a convex polygon with normals of the edges an dot product
 * return the nearest edge e.g 1 for edge p1p2, 2 for edge p2p3, ...
 * or 0 if point is not inside
 */
int point_in_convex_polygon(const Vector2d* vertices, const int vertices_num, const Vector2d& testp);
//int point_in_convex_polygon(const Vector2d& p1, const Vector2d& p2, const Vector2d& p3, const Vector2d& p4, const Vector2d& p5, const Vector2d& testp);

bool point_in_polygon(const Vector2d& test_p, const Vector2d* vertices, const int vertices_num);

// retruns minimal distance squared of one point to multiple other points
float_32 point_min_distsq_to_points(const Vector2d& test_p, const Vector2d* vertices, const int vertices_num);

int ground_point_in_polygon(const Vector2d* vertices, const int vertices_num, const Vector2d& testp);
// rectangle defined by Xmin,Ymin and Xmax,Ymax
bool point_in_rect(const Vector2d& min, const Vector2d& max, const Vector2d& p);

// returns true if at least one point is inside
bool points_in_rect(const Vector2d& min, const Vector2d& max, const Vector2d& p, const Vector2d& q);

// point version of lines_cross
bool lines_cross_points(const Vector2d& p1, const Vector2d& p2, const Vector2d& r1, const Vector2d& r2, Vector2d& interceptP);
// do the lines cross?
// point, vector, point, vector
bool lines_cross(const Vector2d& u, const Vector2d& v, const Vector2d& p, const Vector2d& q, Vector2d& interceptP);

Particle* springs_have_common_particle(const Spring* s1, const Spring* s2);

// only for convex polygons
// returns -1 on error, 0.. for line in polygon particle crossed
int move_particle_out_of_polygon(const Vector2d* vertices, const int vertices_num, Particle* p);

Vector2d get_normal(const Vector2d& v);

Vector2d get_normal_projection(const Vector2d& vectortoproject, const Vector2d& projectionbase);

float_32 get_distance_point_line(const Vector2d& point, const Vector2d& pointline, const Vector2d& vectorline);

float_32 get_distance_point_line_segment(const Vector2d& point, const Vector2d& seg_a, const Vector2d& seg_b);

float_32 get_distance_square_point_line_segment(const Vector2d& point, const Vector2d& seg_a, const Vector2d& seg_b);

void velocity_projection(Particle* p, const Vector2d& v);

Particle* get_nearer_particle(const Vector2d& point, const Spring* s);

Vector2d get_nearer_point(const Vector2d& ref, const Vector2d& a, const Vector2d& b);

void apply_impulse(Spring* deck, Particle* train_point);

int get_nearest_spring(Spring** springs, const int springs_num, Vector2d point);

float_32 pen_depth_point_in_polygon(const Vector2d* poly_vertices, const int poly_vertices_num, const Vector2d* point_vertices);

void normal_force_split(const Vector2d& force_vec, const Vector2d& force_pos,
						const Vector2d& target_1_pos, const Vector2d& target_2_pos,
						Vector2d& result_at_pos_1, Vector2d& result_at_pos_2);
} // namespace Math_utils

#endif

