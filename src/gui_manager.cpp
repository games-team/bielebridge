//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <SDL2/SDL.h>
#include <vector>
#include <cmath>
#include <iostream>

#include "gui_editorsimulator.hpp"
#include "gui_editor.hpp"
#include "gui_simulator.hpp"
#include "gui_manager.hpp"
#include "gui_menu_layouts.hpp"
#include "vector2d.hpp"
#include "spring.hpp"
#include "particle.hpp"
#include "spring.hpp"
#include "level.hpp"
#include "color4f.hpp"
#include "settings.hpp"
#include "error.hpp"


GUI_Manager::GUI_Manager()
{
	gui_menu_start = new GUI_Menu_Start();
	gui_menu_about = new GUI_Menu_About();
	gui_menu_levels = new GUI_Menu_Levels();
	gui_menu_options = new GUI_Menu_Options();

	gui_editor = nullptr;
	gui_simulator = nullptr;

	currentGUI = GUIMENU_START;
}

GUI_Manager::~GUI_Manager()
{
	delete gui_menu_start;
	delete gui_menu_about;
	delete gui_menu_levels;
	delete gui_menu_options;
	if(gui_editor != nullptr ) delete gui_editor;
	if(gui_simulator != nullptr ) delete gui_simulator;
}

void GUI_Manager::draw()
{
	switch(currentGUI)
	{
		case GUIMENU_START:
			gui_menu_start->draw();
			break;
		case GUIMENU_OPTIONS:
			gui_menu_options->draw();
			break;
		case GUIMENU_ABOUT:
			gui_menu_about->draw();
			break;
		case GUIMENU_LEVELS:
			gui_menu_levels->draw();
			break;
		case GUIEDITOR:
			gui_editor->draw();
			break;
		case GUISIMULATOR:
			gui_simulator->draw();
			break;
	}
}

void GUI_Manager::update()
{
	switch(currentGUI)
	{
		case GUIMENU_START: break;
		case GUIMENU_OPTIONS: break;
		case GUIMENU_ABOUT: break;
		case GUIMENU_LEVELS: break;
		case GUIEDITOR:
			gui_editor->update();
			break;
		case GUISIMULATOR:
			gui_simulator->update();
			break;
	}
}

void GUI_Manager::event_mouse_click(int button, int button_state, int x, int y)
{
	switch(currentGUI)
	{
		case GUIMENU_START:
			gui_menu_start->event_mouse_click(this, button, button_state, x, y);
			break;
		case GUIMENU_OPTIONS:
			gui_menu_options->event_mouse_click(this, button, button_state, x, y);
			break;
		case GUIMENU_ABOUT:
			gui_menu_about->event_mouse_click(this, button, button_state, x, y);
			break;
		case GUIMENU_LEVELS:
			gui_menu_levels->event_mouse_click(this, button, button_state, x, y);
			break;
		case GUIEDITOR:
			gui_editor->event_mouse_click(this, button, button_state, x, y);
			break;
		case GUISIMULATOR:
			gui_simulator->event_mouse_click(this, button, button_state, x, y);
			break;
	}
}

void GUI_Manager::event_keyboard(const char* utf8_key)
{
	if(currentGUI == GUISIMULATOR) gui_simulator->event_keyboard(utf8_key);
	if(currentGUI == GUIEDITOR) gui_editor->event_keyboard(this, utf8_key);
	if(currentGUI == GUIMENU_LEVELS) gui_menu_levels->event_keyboard(utf8_key);
}

void GUI_Manager::event_mouse_wheel(int32_t y)
{
	if(currentGUI == GUISIMULATOR) gui_simulator->event_mouse_wheel(y);
	if(currentGUI == GUIEDITOR) gui_editor->event_mouse_wheel(y);
}


void GUI_Manager::event_resize()
{
	switch(currentGUI)
	{
		case GUIMENU_START:
			gui_menu_start->event_window_resize();
			break;
		case GUIMENU_OPTIONS:
			gui_menu_options->event_window_resize();
			break;
		case GUIMENU_ABOUT:
			gui_menu_about->event_window_resize();
			break;
		case GUIMENU_LEVELS:
			gui_menu_levels->event_window_resize();
			break;
		case GUIEDITOR:
			gui_editor->event_window_resize();
			break;
		case GUISIMULATOR:
			gui_simulator->event_window_resize();
			break;
	}
}

void GUI_Manager::createGUI_Editor(const int level_id, const std::string& level_name, const std::string& level_full_path)
{
	if(gui_editor != nullptr ) delete gui_editor;
	gui_editor = new GUI_Editor(level_id, level_name, level_full_path);
}

void GUI_Manager::createGUI_Simulator(Level* level)
{
	if(gui_simulator != nullptr) delete gui_simulator;
	gui_simulator = new GUI_Simulator(level);
}

void GUI_Manager::user_pressed_help()
{
	switch(currentGUI)
	{
		case GUIEDITOR:
			gui_editor->user_pressed_help();
			break;
		case GUISIMULATOR:
// TODO:	gui_simulator->show_help();
			break;
		case GUIMENU_START:
		case GUIMENU_OPTIONS:
		case GUIMENU_ABOUT:
		case GUIMENU_LEVELS:
			break;
	}
}
